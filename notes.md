These are the notes, concepts and plans I wrote during development, edited slightly to be easier to follow

-Braden

# The machine

instruction binary format: `xy aa bb cc dd`

    x  = opcode (0-15)
    y  = argument size (0-4) (little endian)
    aa = argument byte L
    bb = argument byte LM
    cc = argument byte HM
    dd = argument byte H

if size is 0, use default behavior, so 30 (JFR with no argument) means "jump 1 byte forward", same as 31 01

memory consists of two segments: code and data. The data segment shall be referred to as the stack.

    asm  opcode+args     description
    NOP  00              no operation
    ADD  1s LL MM MM HH  add
    SUB  2s LL MM MM HH  subtract
    PSH  3s LL MM MM HH  push to stack (move data segment pointer forward)
    POP  4s LL MM MM HH  pop from stack (move data segment pointer backward)
    JEZ  5s LL MM MM HH  if eq 0 jump by arg else jump to next instruction (1 + s bytes forward)
    JMP  6s LL MM MM HH  jump relative backwards unconditional (has to be unique instruction or else [ will jump to a <)
    GET  70              store stdin char
    PUT  8s LL MM MM HH  put char to stdout
    EXT  90              marks end of program; halt

mappings

    + ADD
    - SUB
    > PSH
    < POP
    [ JEZ offset
    ] JMP offset
    , GET
    . PUT

test program

    +++++[>+++++ +<-]>+++. (prints an !)

compiles to double-linked list (size inst arg depth) (note: I didn't end up using a double linked list, but two
separate linked lists that share the same memory, with a mutual link between matching JEZ/JMP to make it easier to
manage)

    (2 ADD 5 0) ->
    (2 JEZ 0 0) ->
    (1 PSH 1 0) ->
    (2 ADD 6 0) ->
    (1 POP 1 0) ->
    (1 SUB 1 0) ->
    (2 JMP 0 0) ->
    (1 PSH 1 0) ->
    (2 ADD 3 0) ->
    (1 PUT 1 0) ->
    NULL

depth increases every JEZ and decreases every JMP so `[[[]]] = (JEZ 0) (JEZ 1) (JEZ 2) [depth = 3) (JMP 2) (JMP 1) (JMP 0)`.
go over it reading sizes from JEZ. For `JEZ 0 0` find matching `JMP 0 0` and count size aggregate. `JEZ .. JMP = 2+1+2+1+1`
= 7 not counting JMP, 9 counting JMP. size-1-arg-1's will compile to 0-args

`9 <= 255`, so matching JEZ stays 2 bytes (if >255, then size is 3 and +1 to jump amounts)

`7 <= 255`, so matching JMP stays 2 bytes

thus we get our final assembly code:

    ADD 5
    JEZ 9
    PSH
    ADD 6
    POP
    SUB
    JMP 7
    PSH
    ADD 3
    PUT
    EXT

which assembles to

    11 05
    51 09
    30
    11 06
    40
    20
    61 07
    30
    11 03
    80
    90

I also need a header, though.

Binary header: magic + version + pc size + vmem size

    Magic = "BF" + 0xbf
    version = 2 (currently)
    pc size = 3 bytes (little endian, in KiB)
    vmem size = same as pc size

sizes can represent 1KiB - 16GiB (vm max = 1GiB combined)

So the full binary, given 1KiB code segment and 1KiB data segment, as of binary version 2, would look like this:

    42 46 bf 02 01 00 00 01 00 00 11 05 51 09 30 11 06 40 20 61 07 30 11 03 80 90

# The assembly language

same as shown above, but I also want a way to specify code and data segment sizes

    %CODESZ 32
    %DATASZ 32
    ADD 5
    JEZ 9
    PSH
    ADD 6
    POP
    SUB
    JMP 7
    PSH
    ADD 3
    PUT
    EXT

syntax per line: `token [number]`

valid tokens: `#`, `%CODESZ`, `%DATASZ`, `%NOHDR`, `NOP`, `ADD`, `SUB`, `PSH`, `POP`, `JEZ`, `JMP`, `GET`, `PUT`, `EXT`

valid numbers: decimal

do not make assembler smart. If the programmer wants to be stupid and shoot themselves in the foot, let them. assembler
should be easy. Just a rudimentary line-by-line token parser that ignores gibberish. each token maps to an opcode or header information

testing the assembler should be easy as the assembly for `exclamation.bf` is already written out above

caveat: instructions starting with `%` have to appear before other instructions or else they will have no effect.

# The compiler

most of it's covered in the section about the virtual machine. But the compiler does extend the language slightly
while keeping it fully compatible with brainfuck

The compiler will have a preprocessing stage where it checks for `[]` pairs. Once it receives a character other than
`[]` while not inside a `[]` pair, it will stop interpreting `[]` as preprocessor directives

    [ [] ]
    []
    .
    []

the first two outermost pairs are directives.
the . and everything after are to be interpreted as code. So `.[]`

Directives:

    %CODESZ n    - set code segment size (not recommended)
    %DATASZ n    - set data segment size
    %NOEXT       - force the compiler not to emit an EXT
    %NOHDR       - force the compiler not to emit a header (does nothing if the -S switch is active)
    %UNREALISTIC - set unrealistic mode

example: `[%CODESZ 1]` - tells the compiler to set the code size segment to 1KiB

directives with gibberish in them (not starting with %) are to be interpreted as comments.
do not include them, either.
`[]+` should become `(1 ADD 1 0)`, not `(2 JEZ 3)(2 JEZ 2)(1 ADD 1 0)`

I do not have to make the linked list double-ended. jez/jmp pairs can be processed
at jez time and ignored at jmp time when jmps argument is not 0 (meaning it's been set)

Note: I had to add an "unrealistic mode" to make PUT/GET with an argument actually call PUT/GET that many times.
When designing compiler, remember to output the instruction `(80)` n times instead of `(8s nn nn nn nn)`.
The assembler will output the latter, so I added a note about unrealistic mode

Test with ace program (`samples/bf/exclamation-ace.bf`), wikipedia helloworld + rot13, 99 bottles, and lost kingdom

(note: success on every test)

# Compiler: regarding the several edge cases for jump offsets nearby byte representation boundaries

Recall that `JEZ 255` requires 2 bytes while `JEZ 256` requires 3 bytes, and JEZ has to jump the length of its own
instruction, the length of all the instructions between it and its matching JMP, and the length of the matching JMP
(which also varies), and JMP has to jump the inbetween length + JEZ length. As they both affect each other and will
always have differing arguments, this creates a complex situation where you have to time travel or inefficiently go back
and forth several times adjusting jump offsets and arg sizes and jump offsets again to know what to set the size and
jump offsets to to begin with. Not only that, but you cannot simply process them left-to-right, as if the outer jumps
are set and then the inner jumps change in size while being set, then it will throw off the outer jumps. I'll get to
that part later.

To figure out exactly where the edge cases were, I had to simulate it first. I did so on repl.it

```c
#include <stdio.h>

int lim[] = {0, 0, 256, 65536, 16777216};

int *
fn(int base)
{
    static int ret [5];
    int asl = 2;
    int asr = 2;

    while(1){
        if(base + asl >= lim[asr])
            asr++;
        if(base + asl + asr >= lim[asl])
            asl++;
        if(base + asl < lim[asr] && base + asl + asl < lim[asl])
            break;
    }

    ret[0] = base;
    ret[1] = asl;
    ret[2] = asr;
    ret[3] = base + asl + asr;
    ret[4] = base + asl;
    return ret;
}
void
printv(int *v)
{
    printf("%u: %u %u | %u %u\n", v[0], v[1], v[2], v[3], v[4]);
}

int main() {
    for(int i = 250; i < 256; ++i)
        printv(fn(i));
    for(int i = 65520; i < 65536; ++i)
        printv(fn(i));
    for(int i = 16777200; i < 16777216; ++i)
        printv(fn(i));
    return 0;
}
```

the output gave me these edge cases:

    A: B C | D E
    A = input (number of bytes between JEZ and JMP)
    B = JEZ size
    C = JMP size
    D = JEZ arg
    E = JMP arg

    251: 2 2 | 255 253
    252: 3 2 | 257 255
    253: 3 3 | 259 256
    254: 3 3 | 260 257
    255: 3 3 | 261 258
    65529: 3 3 | 65535 65532
    65530: 4 3 | 65537 65534
    65531: 4 3 | 65538 65535
    65532: 4 4 | 65540 65536
    65533: 4 4 | 65541 65537
    65534: 4 4 | 65542 65538
    65535: 4 4 | 65543 65539
    16777207: 4 4 | 16777215 16777211
    16777208: 5 4 | 16777217 16777213
    16777209: 5 4 | 16777218 16777214
    16777210: 5 4 | 16777219 16777215
    16777211: 5 5 | 16777221 16777216
    16777212: 5 5 | 16777222 16777217
    16777213: 5 5 | 16777223 16777218
    16777214: 5 5 | 16777224 16777219
    16777215: 5 5 | 16777225 16777220

so then I wrote a function with them hard-coded (and opted to return both sizes as a single byte):

```c
typedef unsigned char u8;
typedef unsigned int u32;

u8
get_jump_sizes_base(u32 nbytes)
{
    if(nbytes <= 0xff){
        switch(nbytes){
            case 252: return 0x32;
            case 253:
            case 254:
            case 255: return 0x33;
            default:  return 0x22;
        }
    }

    if(nbytes <= 0xffff){
        switch(nbytes){
            case 65530:
            case 65531: return 0x43;
            case 65532:
            case 65533:
            case 65534:
            case 65535: return 0x44;
            default:    return 0x33;
        }
    }

    if(nbytes <= 0xffffff){
        switch(nbytes){
            case 16777208:
            case 16777209:
            case 16777210: return 0x54;
            case 16777211:
            case 16777212:
            case 16777213:
            case 16777214:
            case 16777215: return 0x55;

            default: return 0x44;
        }
    }

    return 0x55;
}
```

and used that to sanity check a better-written function:

```c
u8
get_jump_sizes(u32 nbytes)
{
    static u8 edge_cases[] = {
        /* edge case 252..255 (default 0x22) */
        0x32, 0x33, 0x33, 0x33,

        /* edge case 65530..65535 (default 0x33) */
        0x43, 0x43, 0x44, 0x44, 0x44, 0x44,

        /* edge case 16777208...16777215 (default 0x44) */
        0x54, 0x54, 0x54, 0x55, 0x55, 0x55, 0x55, 0x55
    };
    u8 *edge8 = (edge_cases) - 252;
    u8 *edge16 = (edge_cases + 4) - 65530;
    u8 *edge24 = (edge_cases + 10) - 16777208;

    if(nbytes <= 0xff){
        if(nbytes >= 252)
            return *(edge8 + nbytes);

        return 0x22;
    }

    if(nbytes <= 0xffff){
        if(nbytes >= 65530)
            return *(edge16 + nbytes);

        return 0x33;
    }

    if(nbytes <= 0xffffff){
        if(nbytes >= 16777208)
            return *(edge24 + nbytes);

        return 0x44;
    }

    return 0x55;
}
```

returns both instruction sizes in a single number. `&0xf0` is the mask for the `[`s full size; `&0x0f` for `]`

all of this so the compiler can accurately know in advance how big the instructions should be before it calculates the jump offsets required

# Regarding the complicated situation with nested jumps affecting each other

Processing the jumps inside-out will make it so jumps don't affect each other, since the offsets are all relative.

What I ended up doing was not so dissimilar from an abstract syntax tree after all: I wrote the loop to go inside-out,
recursively. A function is used as the loop control, which takes a starting node from the jump list and a depth (starting
from the depth of the absolute deepest jump) and returns the position in the list, moving right only selecting jumps at
that depth. Once exhausted (finds a null before a match), the function calls itself with `depth - 1` and the first node
of the jump list, starting the process over again until it exhausts the list at depth 0

```c
static struct clnode *
seek_to_depth(struct clnode *current, u32 depth)
{
    if(current == NULL)
        return NULL;

    for(struct clnode *selnode = clist_seek_to_nearest_jez(current); selnode != NULL; selnode = selnode->next_jmp)
        // only accept JEZ instructions at requested depth
        if(selnode->depth == depth && selnode->inst == OP_JEZ)
            return selnode;

    // if we get here, we've exhausted the jump list
    // if the jump list is exhausted and we're at depth 0, then we've gone through every single jump pair
    if(depth == 0)
        return NULL;

    // otherwise, we need to ascend and continue at the start again
    return seek_to_depth(jmplist->head, depth - 1);
}

void
clist_set_jumps(void)
{
    struct clnode *selnode = jmplist->head;
    u32 nbytes;
    u8 sizes;
    u8 largsz;
    u8 rargsz;
    u32 depth = find_deepest_depth();

    // going through every single jump pair, inside-out, left-to-right, recursively
    while((selnode = seek_to_depth(selnode, depth)) != NULL){
        depth = MIN(depth, selnode->depth);
        // first we count the bytes between the JEZ/JMP (argsize + 1 for each)
        nbytes = count_bytes_between(selnode, selnode->match_jmp);
        // then we use that number with that insane time traveling edge case function to figure out what the sizes will be
        sizes = get_jump_sizes(nbytes);
        largsz = (sizes & 0xf0) >> 4;
        rargsz = sizes & 0x0f;
        // then we use those future sizes to calculate the arguments for JEZ (JEZ + nbytes + JMP) and JMP (JEZ + nbytes)
        selnode->arg = nbytes + largsz + rargsz;
        selnode->match_jmp->arg = nbytes + largsz;
        // then we call reset_size, which I wrote because I kept confusing arg size with complete instruction size
        reset_size(selnode);
        reset_size(selnode->match_jmp);
        selnode = selnode->next_jmp;
    }
}
```

See what I mean about having to see into the future? That's what `get_jump_sizes` essentially does. It tells the future so
that JEZ and JMP can have their arguments set before their sizes are set (not that it really matters what order that
happens in at this stage).

There is some redundant stuff in there out of paranoia. Like how depth is set to `MIN(depth, selnode->depth)` even
though `selnode->depth <= depth` is always true. That's what tends to happen when I get overwhelmed with complexity. I
write ultra-safe, no-compromise code full of sanity checks and paranoid redundant constructs.
