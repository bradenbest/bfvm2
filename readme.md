## bfvm2 - a Brainfuck Virtual Machine by Braden Best

This is my second whack at making a brainfuck vm, using what I learned from the design mistakes I made making the
previous version. This distribution comes with:

* bfvm - the vm itself
* bfas - an assembler (bfvm assembly -> bfvm machine code)
* bfc - a compiler (brainfuck -> bfvm assembly or bfvm machine code)
* bfmin - a simple minifier

simply

    $ make

to build all and try running some of the samples

## bfvm executable specification

A specification of the bfvm executable format can be found in bfvm2-executable.md

## What a VM is *not*

Let's just clarify right now that bfvm1 (of the bfvm1 branch) is not an actual VM. It's a JIT compiler with an
overcomplicated bytecode interpreter that just treats the bytecode as if it were brainfuck. So it will seek to the
matching bracket when it encounters one.

Because of brainfuck's similarity to turing machines, people like to call interpreters of the language virtual machines.

I disagree with this terminology on the grounds that BF interpreters are actually *state* machines.

A virtual machine is a program that *emulates* a machine, whereas a typical brainfuck interpreter *simulates* the
machine described by the language spec. Emulation and simulation are very different, and that's where this project is
unique. BFVM does not actually understand brainfuck. Instead, it emulates an imaginary machine with a unique instruction
set, and brainfuck can be *compiled* to a binary executable that the VM understands. The logic of the CPU is very
simple, consisting of a program counter and a stack. There are 10 single-operation instructions that each affect the
stack or program counter in different ways, and it's as simple as that.

The CPU executes the instructions dumbly and is capable of a little more than what brainfuck is capable of. For example,
this program `+[],.` will do nothing, because it will enter an infinite loop, and when compiled, it will be represented
as `10 51 04 61 02 70 80 90`.

The bytecode could be altered:

    10 51 04          61 02 70 80 90
    10 51 04 30 51 04 61 02 70 80 90
             ^^ ^^ ^^

The instructions that were added are `PSH` and `JEZ 4`. This will move the data pointer to a blank cell and then jump
out of the loop without issue. If you attempt something similar in brainfuck, that is, change the program to `+[>[],.`,
the behavior is undefined as the program is invalid. Most likely, the interpreter would attempt to find the matching `]`
and reach the end of the program, probably crashing as a result.

Another example would be Jon Ripley's *Lost Kingdom*. Lost Kingdom is a 2MB brainfuck game, a recreation of the BBC
BASIC classic text adventure. The optimizing compiler that comes with this distribution compresses it down to ~285 KiB
of bytecode. This is not possible in brainfuck, as the smallest possible way to increment a cell 5 times would be
`"+++++"` (5 bytes). Whereas in BFVM machine code, it takes two bytes: `11 05`. In brainfuck, you might generate the
character 'A' and print it with a loop: `"+++++ [> +++++ +++++ +++ <-]."` (24 bytes). In BFVM machine code, it only
takes three bytes: `11 41 80`. Compiled, the 24-byte snippet would become `11 05 51 09 30 11 0D 40 20 61 07 80 90` (13
bytes).
