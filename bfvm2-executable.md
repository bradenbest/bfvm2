This is the specification for the bytecode of bfvm2.

## header

The header consists of 10 bytes:

    3 bytes: magic number (42 46 bf)
    1 bytes: binary format version (currently: 2)
    3 bytes: code segment size (little-endian, in KiB, 0 = 32 KiB)
    3 bytes: data segment size (little-endian, in KiB, 0 = 32 KiB)

## code segment

The design of bfvm1 was terrible, but there was one good idea in there: the byte splitting.

The virtual cpu has 10 opcodes:

       0  NOP
    +  1  ADD
    -  2  SUB
    >  3  PSH
    <  4  POP
    [  5  JEZ
    ]  6  JMP
    ,  7  GET
    .  8  PUT
       9  EXT

Each instruction consists of an opcode (`& 0xf0`) and argument size (`& 0x0f`) in the first byte, and then `size` bytes
for its argument.

bfvm is a 32-bit machine, so sizes greater than 4 will produce unsigned overflow in the argument. Consider 5-f on the
second half of the opcode byte to be gibberish.

arguments can be from 0 - 4 bytes and are interpreted in little-endian byte order. a 0-byte argument is simply: 1.

This creates an effective instruction table:

    sz   op   arg size   description                note
    1    00   0          NOP                        should not appear in code
    1    10   0          ADD 1                      ADD 0 = ADD 1
    2    11   1          ADD 2..255
    3    12   2          ADD 256..65535             nonsense (max cell value = 255)
    4    13   3          ADD 65536..16777215        nonsense
    5    14   4          ADD 16777216..4294967295   nonsense
    1    20   0          SUB 1                      SUB 0 = SUB 1
    2    21   1          SUB 2..255
    3    22   2          SUB 256..65535             nonsense (max cell value = 255)
    4    23   3          SUB 65536..16777215        nonsense
    5    24   4          SUB 16777216..4294967295   nonsense
    1    30   0          PSH 1                      PSH 0 = PSH 1
    2    31   1          PSH 2..255
    3    32   2          PSH 256..65535
    4    33   3          PSH 65536..16777215
    5    34   4          PSH 16777216..4294967295   limit 2^30 (1GiB memory size limit)
    1    40   0          POP 1                      POP 0 = POP 1
    2    41   1          POP 2..255
    3    42   2          POP 256..65535
    4    43   3          POP 65536..16777215
    5    44   4          POP 16777216..4294967295   limit 2^30
    1    50   0          JEZ 1                      nonsense (minimum possible JEZ is 4 bytes)
    2    51   1          JEZ 2..255
    3    52   2          JEZ 256..65535
    4    53   3          JEZ 65536..16777215
    5    54   4          JEZ 16777216..4294967295   limit 2^30
    1    60   0          JMP 1                      nonsense (2 bytes)
    2    61   1          JMP 2..255
    3    62   2          JMP 256..65535
    4    63   3          JMP 65536..16777215
    5    64   4          JMP 16777216..4294967295   limit 2^30
    1    70   0          GET 1                      GET 0 = GET 1
    2    71   1          GET 2..255                 unrealistic mode only
    3    72   2          GET 256..65535             unrealistic mode only
    4    73   3          GET 65536..16777215        unrealistic mode only
    5    74   4          GET 16777216..4294967295   unrealistic mode only
    1    80   0          PUT 1                      PUT 0 = PUT 1
    2    81   1          PUT 2..255                 unrealistic mode only
    3    82   2          PUT 256..65535             unrealistic mode only
    4    83   3          PUT 65536..16777215        unrealistic mode only
    5    84   4          PUT 16777216..4294967295   unrealistic mode only
    1    90   0          EXT                        marks end of program

The ranges `01..0f`, `15..1f`, `25..2f`, `35..3f`, `45..4f`, `55..5f`, `65..6f`, `75..7f`, `85..8f`, and `91..9f` are
definitely nonsense. If this were implemented in real hardware, the instruction set might look like:

    sz   op   arg size   description                note
    1    00   0          NOP                        continue to next instruction
    1    10   0          ADD 1
    2    11   1          ADD 2..255
    1    20   0          SUB 1
    2    21   1          SUB 2..255
    1    30   0          PSH 1
    2    31   1          PSH 2..255
    3    32   2          PSH 256..65535
    4    33   3          PSH 65536..16777215
    5    34   4          PSH 16777216..4294967295
    1    40   0          POP 1
    2    41   1          POP 2..255
    3    42   2          POP 256..65535
    4    43   3          POP 65536..16777215
    5    44   4          POP 16777216..4294967295
    2    51   1          JEZ 2..255
    3    52   2          JEZ 256..65535
    4    53   3          JEZ 65536..16777215
    5    54   4          JEZ 16777216..4294967295
    2    61   1          JMP 2..255
    3    62   2          JMP 256..65535
    4    63   3          JMP 65536..16777215
    5    64   4          JMP 16777216..4294967295
    1    70   0          GET 1
    1    80   0          PUT 1
    1    90   0          EXT                        halt cpu

For a total of 26 unique opcodes.

Unlike bfvm1, the cpu is actually allowed to be dumb here. If it sees `JEZ 20` and the value of the current cell is 0,
the pc *will* jump 20 bytes ahead, no questions asked. This puts the onus on the compiler to do all the heavy lifting,
such as optimizing instructions and finding the correct jump offsets for each JEZ/JMP. All of this combined makes for an
implementation of brainfuck that is fairly efficient both in terms of size and speed.

For instance, lost kingdom, a 2MB game written in brainfuck that is notorious for bringing the typical bf implementation
to its knees, compiles in a fifth of a second and compresses down to about 290KB (a nearly 90% size reduction), and runs
buttery smooth in only 317 KiB of memory (285k for the code segment, 32k for the data segment). Granted, the compiler
requires about 80MB of memory for lost kingdom

By default, the virtual machine allocates 32 KiB for the code segment and 32 KiB for the data segment, although this can
be configured by an executable up to a maximum of 1GiB between both segments. As before, the code and data segments are
contiguous in memory. See `samples/bf/exclamation-ace.bf` for an example of arbitrary code execution. When run, the vm
will execute the instructions in the code segment, run off the end due to the lack of EXT instruction, and eventually
the pc will find itself in the data segment executing instructions that were written by the code segment, producing the
output "!".

Self-modifying code is also possible.
