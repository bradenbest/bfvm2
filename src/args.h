/* usage:
 * args_parse:
 * struct arg_flag flags[] = {
 *     // first entry is used to parse default arguments, is ignored on flag lookups and usage printout
 *     {0, NULL, cb_default, 0},
 *
 *     {'h', "show help", cb_sethelpflag, 0},
 *     ...
 *     {.end = 1}
 * };
 *
 * char **cb_default(char **args){
 *     // in cb_default, args points to the argument itself
 *     return args + 1; // so return args + 1
 * }
 *
 * char **cb_sethelpflag(char **args){
 *     global_show_help = 1;
 *     // in every other one, args points to the first argument of the flag
 *     return args; // so return args + number_of_arguments
 * }
 *
 * char **cb_set_size(char **args){
 *     global_size = atoi(*args);
 *     return args + 1;
 * }
 *
 * you can then call
 * args_parse(argv + 1, flags);
 *
 * args_usage:
 * char *lines[] = {
 *     "my program - does a thing",
 *     "usage: myprog [options] <arg>",
 *     "",
 *     "options:",
 *     NULL,
 *     "",
 *     "other stuff to print",
 *     "you could also just end with two null pointers straight away",
 *     NULL
 * };
 *
 * you can then call
 * args_usage(lines, flags);
 */
#ifndef ARGS_H
#define ARGS_H

#include <stddef.h>

typedef char **(*args_callback_t)(char **args);

struct arg_flag {
    char            flag;
    char *          desc;
    args_callback_t callback;
    int             end;
};

void  args_usage  (char **lines, struct arg_flag *);
void  args_parse  (char **args, struct arg_flag *);

#endif
