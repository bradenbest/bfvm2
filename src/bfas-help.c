#ifndef BFAS_HELP_C
#define BFAS_HELP_C

static char *bfas_help[] = {
    "bfas - brainfuck virtual machine assembler by Braden Best, 2021",
    "usage: bfas [options] <filename>",
    "filename defaults to a.out",
    "",
    "options:",
    NULL,
    NULL
};

static char *bfas_long_help[] = {
    "bfvm2 assembly language details:",
    "",
    "Syntax per line: token [number]",
    "Valid tokens: #, %CODESZ, %DATASZ, %NOHDR, NOP, ADD, SUB, PSH, POP, JEZ, JMP, GET, PUT, EXT",
    "Valid numbers: decimal (32-bit, unsigned)",
    "",
    "Caveats:",
    "* Comments may only appear on their own line.",
    "* A comment at the end of an otherwise valid line will render it invalid.",
    "* Instructions beginning with % are pre-assembly directives and must appear before",
    "  any actual instructions, or else they will have no effect.",
    "* if giving PUT/GET an argument other than 1, the program will only function correctly in 'unrealistic mode'.",
    "  see bfvm's help for more information.",
    "",
    "Tokens:",
    "#          ignore the entire line",
    "%CODESZ n  sets the code segment size to n KiB in the header (max 16777215, max combined 1GiB)",
    "%DATASZ n  sets the data segment size to n KiB in the header (max 16777215, max combined 1GiB)",
    "%NOHDR     tells the assembler not to emit a header",
    "NOP        no operation",
    "ADD [n]    + add n (default=1) to cell",
    "SUB [n]    - subtract n from cell",
    "PSH [n]    > move stack (data segment) pointer n (default=1) bytes forward",
    "POP [n]    < move stack pointer n bytes backward",
    "JEZ n      [ if current cell is zero, jump n bytes forward, else jump to next instruction",
    "JMP n      ] jump back n bytes",
    "GET [n]    , stores character from stdin to cell n (default=1) times.",
    "PUT [n]    . prints character at cell to stdout n (default=1) times.",
    "EXT        marks the end of the program",
    "",
    "Sample program:",
    "",
    "    # sets code and data segments to 1KiB",
    "    %CODESZ 1",
    "    %DATASZ 1",
    "    # program prints a '!'",
    "    ADD 5",
    "    JEZ 9",
    "    PSH",
    "    ADD 6",
    "    POP",
    "    SUB",
    "    JMP 7",
    "    PSH",
    "    ADD 3",
    "    PUT",
    "    EXT",
    "",
    "It is not recommended to write bfvm assembly by hand because you must reason about",
    "argument sizes and specify the jumps correctly.",
    "",
    "This means counting each instruction + the minimum number of bytes needed to",
    "store their arguments, then adding that + the size of JEZ + its argument.",
    "Now take that number and add the size of JMP + its argument, and you have the",
    "argument to JEZ. Subtract JMP's total size and you have the argument to JMP.",
    "However, if either of these offsets exceed 255 (or 65535, or 16777215), then",
    "you have to be a lot more careful specifying their jump offsets.",
    NULL
};

static char *bfas_version_info[] = {
    "bfas v" VERSION_BFAS " (c) 2021 Braden Best",
    "This software is public domain.",
    NULL
};

#endif
