#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "compiler.h"
#include "bfc.h"
#include "cpu.h"
#include "clist.h"
#include "util.h"

static void  macro_codesz       (u32 arg);
static void  macro_datasz       (u32 arg);
static void  macro_noext        (u32 arg);
static void  macro_nohdr        (u32 arg);
static void  macro_unrealistic  (u32 arg);

static compiler_macro_map_callback_t  macro_lookup_by_name  (char *);

struct compiler_inst_map *  get_instruction                (char ch);
static void                 file_seek_to_matching_bracket  (FILE *fin);
static size_t               file_read_to_matching_bracket  (FILE *fin, char *buffer, size_t maxread);
static void                 try_read_macro                 (FILE *fin);
static void                 emit_binary                    (struct clnode *);
static void                 emit_asm                       (struct clnode *);
static void                 emit_code                      (void);

static int has_encountered_non_loop_inst;
static char const bfchars[] = "+-><[],.";
static u32 CFG_CODE_SZ = 32;
static u32 CFG_DATA_SZ = 32;
static u8  CFG_CODE_SZ_MODIFIED;
static u32 CFG_UNREALISTIC;
static u32 CFG_NOEXT;
static u32 CFG_NOHDR;
static int bfc_options[BFC_OPT_END];

static struct compiler_macro_map macro_map[] = {
    { "CODESZ",      macro_codesz, 0 },
    { "DATASZ",      macro_datasz, 0 },
    { "NOEXT",       macro_noext,  0 },
    { "NOHDR",       macro_nohdr,  0 },
    { "UNREALISTIC", macro_unrealistic, 0 },

    {.end = 1}
};

static struct compiler_inst_map instruction_map[8] = {
    {'+', OP_ADD, 1},
    {'-', OP_SUB, 1},
    {'>', OP_PSH, 1},
    {'<', OP_POP, 1},
    {'[', OP_JEZ, 2},
    {']', OP_JMP, 2},
    {',', OP_GET, 1},
    {'.', OP_PUT, 1}
};

static void
macro_codesz(u32 arg)
{
    CFG_CODE_SZ_MODIFIED = 1;
    CFG_CODE_SZ = arg;
}

static void
macro_datasz(u32 arg)
{
    CFG_DATA_SZ = arg;
}

static void
macro_noext(u32 arg)
{
    SILENCE_UNUSED_ARG(arg);
    CFG_NOEXT = 1;
}

static void
macro_nohdr(u32 arg)
{
    SILENCE_UNUSED_ARG(arg);
    CFG_NOHDR = 1;
}

static void
macro_unrealistic(u32 arg)
{
    SILENCE_UNUSED_ARG(arg);
    CFG_UNREALISTIC = 1;
}

static compiler_macro_map_callback_t
macro_lookup_by_name(char *name)
{
    for(struct compiler_macro_map *selmap = macro_map; !selmap->end; ++selmap)
        if(strcmp(selmap->macro, name) == 0)
            return selmap->callback;

    return NULL;
}

struct compiler_inst_map *
get_instruction(char ch)
{
    char *match = strchr(bfchars, ch);
    off_t offset;

    if(match == NULL)
        return NULL;

    offset = match - bfchars;

    return instruction_map + offset;
}

static void
file_seek_to_matching_bracket(FILE *fin)
{
    int ch;
    int depth = 0;

    while((ch = fgetc(fin)) != EOF){
        if(ch == '[')
            ++depth;

        if(ch == ']' && --depth == -1)
            break;
    }
}

static size_t
file_read_to_matching_bracket(FILE *fin, char *buffer, size_t maxread)
{
    int ch;
    int depth = 0;
    char *bufp = buffer;

    while((ch = fgetc(fin)) != EOF){
        if(ch == '[')
            ++depth;

        if(ch == ']' && --depth == -1)
            break;

        if((size_t)(bufp - buffer) < maxread)
            *bufp++ = ch;
    }

    return (size_t)(bufp - buffer);
}

static void
try_read_macro(FILE *fin)
{
    char buffer[KiB];
    int ch = fgetc(fin);
    size_t nread;
    char *macro_txt;
    char *arg_txt;
    compiler_macro_map_callback_t macro;
    u32 arg;

    if(ch != '%'){
        file_seek_to_matching_bracket(fin);
        return;
    }

    nread = file_read_to_matching_bracket(fin, buffer, KiB - 1);
    buffer[nread] = 0;
    macro_txt = strtok(buffer, "[ ]");
    arg_txt = strtok(NULL, "[ ]");

    if(macro_txt == NULL)
        return;

    if((macro = macro_lookup_by_name(buffer)) == NULL)
        return;

    if(arg_txt == NULL)
        arg = 1;
    else
        arg = atou32(arg_txt);

    macro(arg);
}

static void
emit_binary(struct clnode *node)
{
    u8 arg_bytes[4] = {
        (node->arg & 0x000000ff) >>  0,
        (node->arg & 0x0000ff00) >>  8,
        (node->arg & 0x00ff0000) >> 16,
        (node->arg & 0xff000000) >> 24,
    };
    u8 opcode_byte;
    size_t sanity_check = 0;

    opcode_byte = (node->inst << 4) | node->argsize;
    sanity_check += fwrite(&opcode_byte, 1, 1, outfile);
    sanity_check += fwrite(arg_bytes, 1, node->argsize, outfile);

    or_die("compiler.c::emit_binary(): amount written doesn't match instruction size", sanity_check == (u32)(node->argsize + 1));
}

static void
emit_asm(struct clnode *node)
{
    char const *opstr = cpu_opcode_str[node->inst];

    if(node->arg > 1)
        fprintf(outfile, "%s %u\n", opstr, node->arg);
    else
        fprintf(outfile, "%s\n", opstr);
}

static void
emit_code(void)
{
    if(bfc_options[BFC_OPT_ASM]){
        if(CFG_CODE_SZ != 32)
            fprintf(outfile, "%%CODESZ %u\n", CFG_CODE_SZ);

        if(CFG_DATA_SZ != 32)
            fprintf(outfile, "%%DATASZ %u\n", CFG_DATA_SZ);

        if(CFG_NOHDR)
            fprintf(outfile, "%%NOHDR\n");
    } else if(!CFG_NOHDR) {
        outfile_write_header(CFG_CODE_SZ, CFG_DATA_SZ);
    }

    for(struct clnode *selnode = clist_get_normal(); selnode != NULL; selnode = selnode->next){
        if(selnode->inst == OP_NOP)
            continue;

        if(bfc_options[BFC_OPT_ASM])
            emit_asm(selnode);
        else
            emit_binary(selnode);
    }
}

int
compiler_preprocess(FILE *fin)
{
    int ch;

    while((ch = fgetc(fin)) != EOF){
        if(strchr(bfchars, ch) == NULL)
            continue;

        if(ch == '['){
            try_read_macro(fin);
            continue;
        }
    }

    rewind(fin);
    return 1;
}

int
compiler_compile(FILE *fin, FILE *fout)
{
    int ch;
    struct compiler_inst_map *match;

    outfile = fout;

    while((ch = fgetc(fin)) != EOF){
        if(strchr(bfchars, ch) == NULL)
            continue;

        if(ch == '[' && !has_encountered_non_loop_inst){
            file_seek_to_matching_bracket(fin);
            continue;
        }

        has_encountered_non_loop_inst = 1;
        match = get_instruction(ch);
        or_die("compiler.c::compiler_compile() somehow got null pointer from get_instruction()", match != NULL);
        clist_push(match->op, 1);
    }

    if(!CFG_NOEXT)
        clist_push(OP_EXT, 0);
    else
        clist_push(OP_NOP, 0);

    clist_optimize(CFG_UNREALISTIC);
    clist_match_jumps();
    clist_set_jumps();

    if(!CFG_CODE_SZ_MODIFIED)
        CFG_CODE_SZ = (clist_get_total_program_size() / KiB) + 1;

    emit_code();
    clist_free();
    return 1;
}

void
compiler_set_options(int *options)
{
    for(int i = 0; i < BFC_OPT_END; ++i)
        bfc_options[i] = options[i];
}
