#include <stdio.h>
#include <stdlib.h>

#include "cpu.h"
#include "bfvm.h"
#include "args.h"
#include "util.h"
#include "version.h"

static char **  arg_default  (char **);
static char **  arg_h        (char **);
static char **  arg_H        (char **);
static char **  arg_v        (char **);
static char **  arg_V        (char **);
static char **  arg_f        (char **);
static char **  arg_C        (char **);
static char **  arg_D        (char **);
static char **  arg_d        (char **);
static char **  arg_x        (char **);
static char **  arg_u        (char **);

static int bfvm_options[BFVM_OPT_END] = { 0 };
static char *filename = NULL;

static struct arg_flag bfvm_flags[] = {
    {0,   NULL,                              arg_default, 0},

    {'h', "show usage help",                 arg_h, 0},
    {'H', "show long help",                  arg_H, 0},
    {'v', "verbose/debug output",            arg_v, 0},
    {'V', "show version information",        arg_V, 0},
    {'f', "force run (BFVM bytecode only)",  arg_f, 0},
    {'C', "set code segment size (in KiB)",  arg_C, 0},
    {'D', "set data segment size (in KiB)",  arg_D, 0},
    {'d', "disassemble binary to stdout",    arg_d, 0},
    {'x', "dump binary to stdout",           arg_x, 0},
    {'u', "set unrealistic mode",            arg_u, 0},

    {.end = 1}
};

#include "bfvm-help.c"

static char **
arg_default(char **args)
{
    filename = *args;
    return args + 1;
}

static char **
arg_h(char **args)
{
    args_usage(bfvm_help, bfvm_flags);
    // impossible to reach
    return args;
}

static char **
arg_H(char **args)
{
    putsv(bfvm_long_help);
    exit(1);
    // impossible to reach
    return args;
}

static char **
arg_v(char **args)
{
    bfvm_options[BFVM_OPT_VERBOSE] = 1;
    return args;
}

static char **
arg_V(char **args)
{
    putsv(bfvm_version_info);
    exit(1);
    // impossible to reach
    return args;
}

static char **
arg_f(char **args)
{
    bfvm_options[BFVM_OPT_FORCE] = 1;
    return args;
}

static char **
arg_C(char **args)
{
    bfvm_options[BFVM_OPT_IGNORE_FILE_SIZE] = 1;
    cpu_set_size_via_args(atoi(args[0]), 0);
    return args + 1;
}

static char **
arg_D(char **args)
{
    bfvm_options[BFVM_OPT_IGNORE_FILE_SIZE] = 1;
    cpu_set_size_via_args(0, atoi(args[0]));
    return args + 1;
}

static char **
arg_d(char **args)
{
    bfvm_options[BFVM_OPT_DISASSEMBLE] = 1;
    return args;
}

static char **
arg_x(char **args)
{
    bfvm_options[BFVM_OPT_DUMPFILE] = 1;
    return args;
}

static char **
arg_u(char **args)
{
    bfvm_options[BFVM_OPT_UNREALISTIC] = 1;
    return args;
}

void
usage(void)
{
    arg_h(NULL);
}

int
main(int argc, char **argv)
{
    SILENCE_UNUSED_ARG(argc);

    args_parse(argv + 1, bfvm_flags);
    cpu_load_options(bfvm_options);

    if(filename == NULL)
        usage();

    if(bfvm_options[BFVM_OPT_VERBOSE])
        printf("bfvm: Loading file `%s`\n", filename);

    if(bfvm_options[BFVM_OPT_DISASSEMBLE]){
        if(cpu_disassemble_file(filename) == 0){
            puts(cpu_get_error());
            return 1;
        }
    } else if(bfvm_options[BFVM_OPT_DUMPFILE]){
        if(cpu_dump_file(filename) == 0){
            puts(cpu_get_error());
            return 1;
        }
    } else {
        if(cpu_load_file(filename) == 0){
            puts(cpu_get_error());
            return 1;
        }

        cpu_exec();
    }

    return 0;
}
