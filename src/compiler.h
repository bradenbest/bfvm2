#ifndef COMPILER_H
#define COMPILER_H

#include <stdio.h>

#include "cpudefs.h"

typedef void (*compiler_macro_map_callback_t)(u32 arg);

struct compiler_inst_map {
    u8 bfinst;
    u8 op;
    u8 full_size;
};

struct compiler_macro_map {
    char *macro;
    compiler_macro_map_callback_t callback;
    int end;
};

int   compiler_preprocess   (FILE *fin);
int   compiler_compile      (FILE *fin, FILE *fout);
void  compiler_set_options  (int *options);

#endif
