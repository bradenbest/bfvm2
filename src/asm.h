#ifndef ASM_H
#define ASM_H

#include <stdio.h>

#include "cpudefs.h"

typedef void (*asm_map_callback_t)(u32 arg);

struct asm_map {
    char *             asm_str;
    asm_map_callback_t callback;

    int end;
};

extern struct asm_map asm_lookup_table[];

struct asm_map *  asm_lookup_by_name  (char *inst);
int               asm_assemble        (char *filename, FILE *fout);

#endif
