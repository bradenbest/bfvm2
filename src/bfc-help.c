#ifndef BFC_HELP_C
#define BFC_HELP_C

static char *bfc_help[] = {
    "bfc - optimizing brainfuck compiler by Braden Best, 2021",
    "usage: bfc [options] <file>",
    "",
    "options:",
    NULL,
    "",
    "Compiles brainfuck source code to bfvm machine code.",
    NULL
};

static char *bfc_long_help[] = {
    "The brainfuck source code can contain preprocessor directives at the top of the file.",
    "",
    "Preprocessor directives MUST be in the format:",
    "",
    "    [%token argument]",
    "    or",
    "    [%token]",
    "",
    "directives:",
    "%CODESZ n     set code segment size to n KiB (not recommended, the compiler should decide this)",
    "%DATASZ n     set data segment size to n KiB",
    "%NOEXT        force the compiler not to emit an EXT instruction",
    "%NOHDR        force the compiler not to emit a header (emits %NOHDR line if the -S switch is active)",
    "%UNREALISTIC  set unrealistic mode (minimizes `,` and `.`; see bfvm help on unrealistic mode)",
    "",
    "Directives MUST be at the top of the file. The compiler will optimize immediate",
    "0-loops out of the emitted code, but it can only do that until the first non-loop instruction.",
    "",
    "Sample program:",
    "",
    "    [prints an !]",
    "    [%DATASZ 1]",
    "    [%NOHDR]",
    "    +++++[>+++++ +<-]>+++. (prints an !)",
    "",
    "The two directives will be applied, and the compiler proper will only see",
    "",
    "    +++++[>++++++<-]>+++.",
    "",
    "If, on the other hand, the program looks like this:",
    "",
    "    [prints an !]",
    "    +++++[>+++++ +<-]>+++.",
    "    [%DATASZ 1]",
    "    [%NOHDR]",
    "",
    "It will still compile, but the directives will be ignored and the code will be interpreted as",
    "",
    "    +++++[>++++++<-]>+++.[][]",
    "",
    "so it will enter an infinite loop",
    "",
    "Naturally, the input brainfuck code must be syntactically correct. If the number",
    "and/or orientations of '[' and ']' instructions are unbalanced or nonsensical,",
    "the behavior is undefined. The following program, for example:",
    "",
    "    +][",
    "",
    "will cause the compiler to throw an error, because the final pass assumes that every",
    "jump instruction will have a `node->match_jmp` that is not `NULL`. So in this",
    "case, the sanity check will fail and the compiler will abort.",
    NULL
};

static char *bfc_version_info[] = {
    "bfc v" VERSION_BFC " (c) 2021 Braden Best",
    "This software is public domain.",
    NULL
};

#endif
