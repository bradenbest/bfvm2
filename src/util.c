#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "util.h"

FILE *outfile;

u32
atou32(char const *str)
{
    u32 out = 0;

    for(; *str; ++str)
        out = (10 * out) + (*str - '0');

    return out;
}

char **
putsv(char **sv)
{
    for(; *sv != NULL; ++sv)
        puts(*sv);

    return sv;
}

u8
get_argsz(u32 arg)
{
    if(arg == 0 || arg == 1)
        return 0;

    if(arg <= 0xff)
        return 1;

    if(arg <= 0xffff)
        return 2;

    if(arg <= 0xffffff)
        return 3;

    return 4;
}

void
outfile_write_header(u32 code_segment_size, u32 data_segment_size)
{
    /* force little-endian byte order */
    u8 codesz_bytes[3] = {
        (code_segment_size & 0x000000ff) >>  0,
        (code_segment_size & 0x0000ff00) >>  8,
        (code_segment_size & 0x00ff0000) >> 16,
    };
    u8 datasz_bytes[3] = {
        (data_segment_size & 0x000000ff) >>  0,
        (data_segment_size & 0x0000ff00) >>  8,
        (data_segment_size & 0x00ff0000) >> 16,
    };
    u8 version = BINARY_FMT_VERSION;
    size_t sanity_check = 0;

    sanity_check += fwrite(BINARY_FMT_MAGIC, 1, 3, outfile);
    sanity_check += fwrite(&version, 1, 1, outfile);
    sanity_check += fwrite(codesz_bytes, 1, 3, outfile);
    sanity_check += fwrite(datasz_bytes, 1, 3, outfile);

    if(sanity_check != 10){
        fprintf(stderr, "FATAL: util.c::outfile_write_header() failed sanity check (expected 10 bytes, got %lu bytes)\n", sanity_check);
        exit(2);
    }
}

void
file_seek_to(FILE *fp, char sentinel)
{
    int ch;

    while((ch = fgetc(fp)) != EOF)
        if(ch == sentinel)
            return;
}

void
or_die(char const *msg, int expr)
{
    if(!expr){
        fputs(msg, stderr);
        exit(1);
    }
}
