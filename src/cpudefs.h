#ifndef CPUDEFS_H
#define CPUDEFS_H

#include <stdint.h>

#define KiB (1L << 10)
#define MiB (1L << 20)
#define GiB (1L << 30)

#define BINARY_FMT_MAGIC    ("BF\xbf")
#define BINARY_FMT_VERSION  (0x02)

/* Sizes; See bfvm2-executable.md for header specification */
#define BINARY_FMT_HEADER_SIZE  (0x0a)
#define BINARY_FMT_MAGIC_SIZE   (0x03)
#define BINARY_FMT_VERSION_SIZE (0x01)
#define BINARY_FMT_SIZE_SIZE    (0x06)
#define BINARY_FMT_CODESZ_SIZE  (0x03)
#define BINARY_FMT_DATASZ_SIZE  (0x03)

/* Offsets */
#define BINARY_FMT_MAGIC_OFFSET   (0x00)
#define BINARY_FMT_VERSION_OFFSET (0x03)
#define BINARY_FMT_SIZE_OFFSET    (0x04)
#define BINARY_FMT_CODESZ_OFFSET  (0x04)
#define BINARY_FMT_DATASZ_OFFSET  (0x07)

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;

enum opcodes {
    OP_NOP,
    OP_ADD,
    OP_SUB,
    OP_PSH,
    OP_POP,
    OP_JEZ,
    OP_JMP,
    OP_GET,
    OP_PUT,
    OP_EXT,
    OP_END
};

#endif
