#include <stdlib.h>

#include "clist.h"
#include "compiler.h"
#include "cpu.h"
#include "util.h"

#define INSTSZ(node) \
    ((node)->argsize + 1)

#define IS_JUMP(node) \
    ( (node)->inst == OP_JEZ || (node)->inst == OP_JMP )

static u8              get_jump_sizes                  (u32 nbytes);
static void            push_jmp                        (struct clnode *);
static void            sanity_check_clist_match_jumps  (void);
static void            reset_size                      (struct clnode *);
static u32             count_bytes_between             (struct clnode *start, struct clnode *end);
static u32             find_deepest_depth              (void);
static struct clnode * seek_to_depth                   (struct clnode *current, u32 depth);
static struct clnode * seek_to_nearest_jez             (struct clnode *current);

static struct clist list[1];
static struct clist jmplist[1];
static u32 jmp_depth;

/* this function solves an extremely finnicky problem involving three
 * nontrivial edge cases. If you would like to take a crack at writing
 * a more efficient function, sanity check it against this version first
 * using the inputs 0..0x01000000 (0..16777216)
 * returns full instruction sizes for JEZ (&0xf0) and JMP (&0x0f)
 */
static u8
get_jump_sizes(u32 nbytes)
{
    static u8 edge_cases[] = {
        /* edge case 252..255 (default 0x22) */
        0x32, 0x33, 0x33, 0x33,

        /* edge case 65530..65535 (default 0x33) */
        0x43, 0x43, 0x44, 0x44, 0x44, 0x44,

        /* edge case 16777208...16777215 (default 0x44) */
        0x54, 0x54, 0x54, 0x55, 0x55, 0x55, 0x55, 0x55
    };
    u8 *edge8 = (edge_cases) - 252;
    u8 *edge16 = (edge_cases + 4) - 65530;
    u8 *edge24 = (edge_cases + 10) - 16777208;

    if(nbytes <= 0xff){
        if(nbytes >= 252)
            return *(edge8 + nbytes);

        return 0x22;
    }

    if(nbytes <= 0xffff){
        if(nbytes >= 65530)
            return *(edge16 + nbytes);

        return 0x33;
    }

    if(nbytes <= 0xffffff){
        if(nbytes >= 16777208)
            return *(edge24 + nbytes);

        return 0x44;
    }

    return 0x55;
}

static void
push_jmp(struct clnode *newnode)
{
    if(newnode->inst == OP_JEZ)
        newnode->depth = jmp_depth++;

    if(newnode->inst == OP_JMP)
        newnode->depth = --jmp_depth;

    if(jmplist->head == NULL){
        jmplist->head = newnode;
        jmplist->tail = newnode;
        return;
    }

    jmplist->tail->next_jmp = newnode;
    jmplist->tail = newnode;
}

static void
sanity_check_clist_match_jumps(void)
{
    for(struct clnode *selnode = jmplist->head; selnode != NULL; selnode = selnode->next_jmp)
        or_die("clist.c::sanity_check_clist_match_jumps(): One or more [] instructions have no match.", selnode->match_jmp != NULL);
}

/* resets the argument size after argument is modified
 * There used to be a JEZ/JMP check that would force them to be size 1
 * JEZ and JMP cannot possibly have an arg < 2, though, so it was removed
 */
static void
reset_size(struct clnode *node)
{
    node->argsize = get_argsz(node->arg);
}

static u32
count_bytes_between(struct clnode *start, struct clnode *end)
{
    u32 total_bytes = 0;

    for(struct clnode *selnode = start->next; selnode != end; selnode = selnode->next)
        total_bytes += INSTSZ(selnode);

    return total_bytes;
}

static u32
find_deepest_depth(void)
{
    u32 depth = 0;

    for(struct clnode *selnode = jmplist->head; selnode != jmplist->tail; selnode = selnode->next)
        depth = MAX(selnode->depth, depth);

    return depth;
}

/* similar to seek_to_nearest_jez, but it only returns a JEZ at the requested depth
 * this makes it possible to walk the list recursively inside-out.
 */
static struct clnode *
seek_to_depth(struct clnode *current, u32 depth)
{
    if(current == NULL)
        return NULL;

    for(struct clnode *selnode = seek_to_nearest_jez(current); selnode != NULL; selnode = seek_to_nearest_jez(selnode->next_jmp))
        if(selnode->depth == depth)
            return selnode;

    if(depth == 0)
        return NULL;

    return seek_to_depth(jmplist->head, depth - 1);
}

/* if current instruction is in the jump list, it will return the nearest JEZ
 * if current is already a JEZ, then current is returned
 * if current is not in the jump list (Not a JEZ or JMP), then it's reset to the
 * first node in the jump list
 */
static struct clnode *
seek_to_nearest_jez(struct clnode *current)
{
    if(current == NULL)
        return NULL;

    if(!IS_JUMP(current))
        return seek_to_nearest_jez(jmplist->head);

    /* current is now guaranteed to be a jump */

    if(current->inst == OP_JEZ)
        return current;

    for(; current->next_jmp != NULL; current = current->next_jmp)
        if(current->inst == OP_JEZ)
            return current;

    return NULL;
}


void
clist_free(void)
{
    struct clnode *next;

    for(struct clnode *selnode = list->head; selnode != NULL; selnode = next){
        next = selnode->next;
        free(selnode);
    }
}

int
clist_push(u8 inst, u32 arg)
{
    struct clnode *newnode = malloc(sizeof *newnode);

    or_die("clist.c::clist_push() could not allocate memory", newnode != NULL);

    newnode->inst = inst;
    newnode->arg = arg;
    reset_size(newnode);
    newnode->depth = 0;

    newnode->next = NULL;
    newnode->next_jmp = NULL;
    newnode->match_jmp = NULL;

    if(list->head == NULL){
        /* it's impossible for the first instruction to be [ or ] */
        list->head = newnode;
        list->tail = newnode;
        return 1;
    }

    list->tail->next = newnode;
    list->tail = newnode;

    if(inst == OP_JEZ || inst == OP_JMP)
        push_jmp(newnode);

    return 1;
}

/* goes through op list and merges duplicate instructions */
void
clist_optimize(u32 unrealisticmode)
{
    struct clnode *delete_node;

    for(struct clnode *selnode = list->head; selnode != NULL; selnode = selnode->next){
        /* skip `[` and `]` since they are not to be collapsed */
        if(selnode->inst == OP_JEZ || selnode->inst == OP_JMP)
            continue;

        /* skip `,` and `.` but only if unrealistic mode is inactive. */
        if( (selnode->inst == OP_GET || selnode->inst == OP_PUT) && !unrealisticmode)
            continue;

        while(selnode->next != NULL && selnode->inst == selnode->next->inst){
            delete_node = selnode->next;
            selnode->next = delete_node->next;
            selnode->arg += 1;
            reset_size(selnode);
            free(delete_node);
        }
    }
}

/* goes through jump list and sets the jumps appropriately
 * This function was a motherfucker to write
 *
 * depth used to be set to MIN(depth, selnode->depth), but this was
 * redundant as seek_to_depth only returns a node where node depth <= input depth
 *
 * the while loop uses `seek_to_depth` to perform a recursive inside-out, left-to-right
 * walk of the entire jump list, counting the bytes between each jump pair and using
 * `get_jump_sizes` to predict the sizes of each jump so it can properly set their
 * arguments. See notes.md for a little more elaboration on what makes this so complex
 */
void
clist_set_jumps(void)
{
    struct clnode *selnode = jmplist->head;
    u32 nbytes;
    u8 sizes;
    u8 largsz;
    u8 rargsz;
    u32 depth = find_deepest_depth();

    while((selnode = seek_to_depth(selnode, depth)) != NULL){
        depth = selnode->depth;
        nbytes = count_bytes_between(selnode, selnode->match_jmp);
        sizes = get_jump_sizes(nbytes);
        largsz = (sizes & 0xf0) >> 4;
        rargsz = sizes & 0x0f;
        selnode->arg = nbytes + largsz + rargsz;
        selnode->match_jmp->arg = nbytes + largsz;
        reset_size(selnode);
        reset_size(selnode->match_jmp);
        selnode = selnode->next_jmp;
    }
}

void
clist_print(struct clnode *node)
{
    if(node->inst == OP_JEZ || node->inst == OP_JMP)
        printf("(%s %u) size=%u depth=%u\n", cpu_opcode_str[node->inst], node->arg, INSTSZ(node), node->depth);
    else
        printf("(%s %u) size=%u\n", cpu_opcode_str[node->inst], node->arg, INSTSZ(node));
}

void
clist_print_all(void)
{
    for(struct clnode *selnode = list->head; selnode != NULL; selnode = selnode->next)
        clist_print(selnode);
}

u32
clist_get_total_program_size(void)
{
    return count_bytes_between(list->head, NULL) + INSTSZ(list->head);
}

/* goes through jump list and binds matching JEZ/JMP pairs directly to each other */
void
clist_match_jumps(void)
{
    for(struct clnode *selnode = jmplist->head; selnode != jmplist->tail; selnode = selnode->next_jmp){
        if(selnode->inst == OP_JMP)
            continue;

        for(struct clnode *search = selnode; 1; search = search->next_jmp){
            if(search->depth == selnode->depth && search->inst == OP_JMP){
                selnode->match_jmp = search;
                search->match_jmp = selnode;
                break;
            }
        }
    }

    sanity_check_clist_match_jumps();
}

struct clnode *
clist_get_normal(void)
{
    return list->head;
}

struct clnode *
clist_get_jumps(void)
{
    return jmplist->head;
}
