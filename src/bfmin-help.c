#ifndef BFMIN_HELP_C
#define BFMIN_HELP_C

static char *bfmin_help[] = {
    "bfmin - minify brainfuck code",
    "usage: bfmin [file]",
    "",
    "options:",
    NULL,
    "",
    "If no file is provided, bfmin will pull its input from stdin.",
    NULL
};

// bfmin_long_help not implemented

static char *bfmin_version_info[] = {
    "bfmin v" VERSION_BFMIN " (c) 2021 Braden Best",
    "This software is public domain.",
    NULL
};

#endif
