#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "asm.h"
#include "util.h"

static ssize_t  file_read_line        (char *buffer, size_t maxsz, FILE *fp);
static void     parse_line            (char *line, size_t nread);
static void     outfile_write         (u8 op, u32 arg);

static void  emit_codesz  (u32 arg);
static void  emit_datasz  (u32 arg);
static void  emit_nohdr   (u32 arg);
static void  emit_nop     (u32 arg);
static void  emit_add     (u32 arg);
static void  emit_sub     (u32 arg);
static void  emit_psh     (u32 arg);
static void  emit_pop     (u32 arg);
static void  emit_jez     (u32 arg);
static void  emit_jmp     (u32 arg);
static void  emit_get     (u32 arg);
static void  emit_put     (u32 arg);
static void  emit_ext     (u32 arg);

static u32 ASM_DATA_SZ = 32; // in KiB
static u32 ASM_CODE_SZ = 32;
static int FLAG_NOHDR;

struct asm_map asm_lookup_table[] = {
    { "%CODESZ", emit_codesz, 0 },
    { "%DATASZ", emit_datasz, 0 },
    { "%NOHDR",  emit_nohdr, 0 },
    { "NOP",     emit_nop, 0 },
    { "ADD",     emit_add, 0 },
    { "SUB",     emit_sub, 0 },
    { "PSH",     emit_psh, 0 },
    { "POP",     emit_pop, 0 },
    { "JEZ",     emit_jez, 0 },
    { "JMP",     emit_jmp, 0 },
    { "GET",     emit_get, 0 },
    { "PUT",     emit_put, 0 },
    { "EXT",     emit_ext, 0 },

    { .end = 1 }
};

static ssize_t
file_read_line(char *buffer, size_t maxsz, FILE *fp)
{
    int ch;
    char *bufp = buffer;

    while((ch = fgetc(fp)) != EOF){
        if(ch == '\n')
            break;

        /* ignore the rest of the line*/
        if((size_t)(bufp - buffer) == maxsz - 1){
            file_seek_to(fp, '\n');
            break;
        }

        *bufp++ = ch;
    }

    if(ch == EOF)
        return -1;

    *bufp = 0;
    return (ssize_t)(bufp - buffer);
}

static void
parse_line(char *line, size_t nread)
{
    char *linenws;
    char *tok_name = strtok(line, " ");
    char *tok_arg = strtok(NULL, " ");
    struct asm_map *match;
    u32 arg;

    /* empty line */
    if(nread == 0)
        return;

    /* # comment */
    if((linenws = strchr(line, '#')) != NULL)
        return;

    /* commands (gibberish is ignored) */
    match = asm_lookup_by_name(tok_name);

    if(match == NULL)
        return;

    if(tok_arg == NULL)
        arg = 0;
    else
        arg = atou32(tok_arg);

    match->callback(arg);
}

static void
outfile_write(u8 op, u32 arg)
{
    static int initialized = 0;
    u8 arg_bytes[4] = {
        (arg & 0x000000ff) >>  0,
        (arg & 0x0000ff00) >>  8,
        (arg & 0x00ff0000) >> 16,
        (arg & 0xff000000) >> 24,
    };
    u8 arg_sz = get_argsz(arg);
    u8 op_byte = (op << 4) | arg_sz;
    size_t sanity_check = 0;

    if(!initialized && !FLAG_NOHDR){
        outfile_write_header(ASM_CODE_SZ, ASM_DATA_SZ);
        initialized = 1;
    }

    sanity_check += fwrite(&op_byte, 1, 1, outfile);
    sanity_check += fwrite(arg_bytes, 1, arg_sz, outfile);

    if((u8)sanity_check != arg_sz + 1){
        fprintf(stderr, "FATAL: asm.c::outfile_write() failed sanity check (expected %u bytes, got %lu bytes)\n", arg_sz + 1, sanity_check);
        exit(2);
    }
}

/* callbacks */

static void
emit_codesz(u32 arg)
{
    ASM_CODE_SZ = arg;
}

static void
emit_datasz(u32 arg)
{
    ASM_DATA_SZ = arg;
}

static void
emit_nohdr(u32 arg)
{
    SILENCE_UNUSED_ARG(arg);
    FLAG_NOHDR = 1;
}

static void
emit_nop(u32 arg)
{
    outfile_write(OP_NOP, arg);
}

static void
emit_add(u32 arg)
{
    outfile_write(OP_ADD, arg);
}

static void
emit_sub(u32 arg)
{
    outfile_write(OP_SUB, arg);
}

static void
emit_psh(u32 arg)
{
    outfile_write(OP_PSH, arg);
}

static void
emit_pop(u32 arg)
{
    outfile_write(OP_POP, arg);
}

static void
emit_jez(u32 arg)
{
    outfile_write(OP_JEZ, arg);
}

static void
emit_jmp(u32 arg)
{
    outfile_write(OP_JMP, arg);
}

static void
emit_get(u32 arg)
{
    outfile_write(OP_GET, arg);
}

static void
emit_put(u32 arg)
{
    outfile_write(OP_PUT, arg);
}

static void
emit_ext(u32 arg)
{
    outfile_write(OP_EXT, arg);
}

/* public */

struct asm_map *
asm_lookup_by_name(char *inst)
{
    for(struct asm_map *selmap = asm_lookup_table; !selmap->end; ++selmap)
        if(strcmp(inst, selmap->asm_str) == 0)
            return selmap;

    return NULL;
}

int
asm_assemble(char *filename, FILE *fout)
{
    char linebuf[4096];
    FILE *fp;
    ssize_t nread;

    outfile = fout;

    if(filename == NULL)
        return 0;

    if((fp = fopen(filename, "r")) == NULL)
        return 0;

    if(outfile == NULL)
        outfile = stdout;

    while((nread = file_read_line(linebuf, 4096, fp)) != -1)
        parse_line(linebuf, nread);

    fclose(fp);
    return 1;
}
