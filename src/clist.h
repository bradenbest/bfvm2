/* >clist_push when reading source code
 * >clist_match_jumps after finished reading
 * the rest is WIP, may not need clist_get_jumps, but will need a way after clist_match_jumps to set the arguments of
 * each JEZ/JMP before calling clist_get_normal and writing everything to the file
 */
#ifndef CLIST_H
#define CLIST_H

#include "cpudefs.h"

struct clnode{
    u8  inst;
    u8  argsize;
    u32 arg;
    u32 depth;

    struct clnode *next;
    struct clnode *next_jmp;
    struct clnode *match_jmp;
};

struct clist {
    struct clnode *head;
    struct clnode *tail;
};

void             clist_free                    (void);
int              clist_push                    (u8 inst, u32 arg);
void             clist_optimize                (u32 unrealisticmode);
void             clist_set_jumps               (void);
void             clist_print                   (struct clnode *);
void             clist_print_all               (void);
u32              clist_get_total_program_size  (void);
void             clist_match_jumps             (void);
struct clnode *  clist_get_normal              (void);
struct clnode *  clist_get_jumps               (void);

#endif
