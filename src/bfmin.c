#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "util.h"
#include "args.h"
#include "version.h"

static char **  arg_default  (char **);
static char **  arg_h        (char **);
static char **  arg_v        (char **);

static void  seek_to_closing_bracket  (FILE *);
static int   minify                   (FILE *);

static char *filename;

static struct arg_flag bfmin_flags[] = {
    {0, NULL, arg_default, 0},

    {'h', "show help",                arg_h, 0},
    {'v', "show version information", arg_v, 0},

    {.end = 1}
};

#include "bfmin-help.c"

static char **
arg_default(char **args)
{
    filename = *args;
    return args + 1;
}

static char **
arg_h(char **args)
{
    args_usage(bfmin_help, bfmin_flags);
    exit(1);
    // impossible to reach
    return args;
}

static char **
arg_v(char **args)
{
    putsv(bfmin_version_info);
    exit(1);
    // impossible to reach
    return args;
}

/* eats characters until it gets out of the given loop
 * assumes file stream is already inside the loop
 * leaves file stream just after end of loop
 */
static void
seek_to_closing_bracket(FILE *f)
{
    int ch;
    int depth = 0;

    while((ch = fgetc(f)) != EOF){
        if(ch == '[')
            ++depth;

        if(ch == ']'){
            --depth;

            if(depth == -1)
                return;
        }
    }
}

/* strips non-bf characters and no-op loops
 * a similar optimization is planned for bfc in a future commit
 */
static int
minify(FILE *f)
{
    int ch;
    int first_write = 0;
    int last_ch = 0;

    if(f == NULL)
        return 0;

    while((ch = fgetc(f)) != EOF){
        /* strip non-bf characters */
        if(strchr("+-><[],.", ch) == NULL)
            continue;

        /* strip useless loops (immediately follows another loop) */
        if(last_ch == ']' && ch == '[' ){
            seek_to_closing_bracket(f);
            continue;
        }

        if(first_write == 0){
            /* strip useless loops (cells haven't been modified) */
            if(ch == '['){
                seek_to_closing_bracket(f);
                continue;
            }

            if(strchr("+-,", ch) != NULL)
                first_write = 1;
        }

        putchar(ch);
        last_ch = ch;
    }

    return 1;
}

int
main(int argc, char **argv)
{
    SILENCE_UNUSED_ARG(argc);
    FILE *f;

    args_parse(argv + 1, bfmin_flags);
    f = fopen_safe(filename, "r");

    if(!minify(f))
        minify(stdin);

    if(f != NULL)
        fclose(f);

    return 0;
}
