#include <stdio.h>
#include <stdlib.h>

#include "compiler.h"
#include "bfc.h"
#include "args.h"
#include "util.h"
#include "version.h"

static char **  arg_default  (char **);
static char **  arg_h        (char **);
static char **  arg_H        (char **);
static char **  arg_v        (char **);
static char **  arg_S        (char **);
static char **  arg_o        (char **);

static int options[BFC_OPT_END];
static char *infilename = NULL;
static char *outfilename = "a.out";
static int OUTFILE_HAS_BEEN_SET;

static struct arg_flag bfc_flags[] = {
    {0, NULL, arg_default, 0},

    {'h', "show usage help",                             arg_h, 0},
    {'H', "show long help",                              arg_H, 0},
    {'v', "show version information",                    arg_v, 0},
    {'S', "output assembly",                             arg_S, 0},
    {'o', "set output filename (default = a.out / a.s)", arg_o, 0},

    {.end = 1}
};

#include "bfc-help.c"

static char **
arg_default(char **args)
{
    infilename = *args;
    return args + 1;
}

static char **
arg_h(char **args)
{
    args_usage(bfc_help, bfc_flags);
    // impossible to reach
    return args;
}

static char **
arg_H(char **args)
{
    putsv(bfc_long_help);
    exit(1);
    // impossible to reach
    return args;
}

static char **
arg_v(char **args)
{
    putsv(bfc_version_info);
    exit(1);
    // impossible to reach
    return args;
}

static char **
arg_S(char **args)
{
    options[BFC_OPT_ASM] = 1;

    if(!OUTFILE_HAS_BEEN_SET)
        outfilename = "a.s";

    return args;
}

static char **
arg_o(char **args)
{
    outfilename = *args;
    OUTFILE_HAS_BEEN_SET = 1;
    return args + 1;
}

void
usage(void)
{
    arg_h(NULL);
}

int
main(int argc, char **argv)
{
    SILENCE_UNUSED_ARG(argc);
    FILE *fin;
    FILE *fout;

    args_parse(argv + 1, bfc_flags);

    if(infilename == NULL)
        usage();

    fout = fopen(outfilename, "wb");

    if(fout == NULL){
        printf("Could not open output file `%s`\n", outfilename);
        return 1;
    }

    if(infilename == NULL || (fin = fopen(infilename, "r")) == NULL)
        return 1;

    compiler_set_options(options);

    if(!compiler_preprocess(fin))
        return 1;

    if(!compiler_compile(fin, fout))
        return 1;

    fclose(fin);
    fclose(fout);
    return 0;
}
