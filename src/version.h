#ifndef VERSION_H
#define VERSION_H

#define VERSION_BFC   "1.1.0"
#define VERSION_BFAS  "1.0.0"
#define VERSION_BFVM  "2.2.2"
#define VERSION_BFMIN "1.0.0"

#endif
