#include <stdio.h>
#include <stdlib.h>

#include "asm.h"
#include "args.h"
#include "util.h"
#include "version.h"

static char **  arg_default  (char **args);
static char **  arg_h        (char **args);
static char **  arg_H        (char **args);
static char **  arg_v        (char **args);
static char **  arg_o        (char **args);

static char *input_filename = NULL;
static char *output_filename = "a.out";

static struct arg_flag bfas_flags[] = {
    {0, NULL, arg_default, 0},

    {'h', "show usage help",                       arg_h, 0},
    {'H', "show long help",                        arg_H, 0},
    {'v', "show version information",              arg_v, 0},
    {'o', "set output filename (default = a.out)", arg_o, 0},

    {.end = 1}
};

#include "bfas-help.c"

static char **
arg_default(char **args)
{
    input_filename = *args;
    return args + 1;
}

static char **
arg_h(char **args)
{
    args_usage(bfas_help, bfas_flags);
    // impossible to reach
    return args;
}

static char **
arg_H(char **args)
{
    putsv(bfas_long_help);
    exit(1);
    // impossible to reach
    return args;
}

static char **
arg_v(char **args)
{
    putsv(bfas_version_info);
    exit(1);
    // impossible to reach
    return args;
}

static char **
arg_o(char **args)
{
    output_filename = *args;
    return args + 1;
}

void
usage(void)
{
    arg_h(NULL);
}

int
main(int argc, char **argv)
{
    SILENCE_UNUSED_ARG(argc);
    FILE *fout;

    args_parse(argv + 1, bfas_flags);

    if(input_filename == NULL)
        usage();

    fout = fopen(output_filename, "wb");

    if(fout == NULL)
        return 1;

    if(!asm_assemble(input_filename, fout))
        return 1;

    fclose(fout);
    return 0;
}
