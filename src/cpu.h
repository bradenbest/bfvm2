#ifndef CPU_H
#define CPU_H

#include <stddef.h>

#include "cpudefs.h"

#define PMEM_SZ ((cpu_pcnt_sz) + (cpu_vmem_sz))
#define PCNT_SZ_DEFAULT (32 * (KiB))
#define VMEM_SZ_DEFAULT (32 * (KiB))

enum cpu_errno {
    CPUERR_OK,
    CPUERR_NOFILE,
    CPUERR_FOPENFAIL,
    CPUERR_BADHEADER,
    CPUERR_OLDVERSION,
    CPUERR_FERROR,
    CPUERR_TOOBIG,
    CPUERR_MALLOCFAIL,
    CPUERR_END
};

extern size_t cpu_pcnt_sz;
extern size_t cpu_vmem_sz;
extern char * cpu_opcode_str[];

int           cpu_init                  (void);
int           cpu_check_header          (char *);
int           cpu_check_header_version  (char *);
int           cpu_set_size_via_header   (char *);
int           cpu_set_size_via_args     (size_t pcnt_sz_kib, size_t vmem_sz_kib);
char const *  cpu_get_error             (void);
int           cpu_load_file             (char *);
int           cpu_disassemble_file      (char *);
int           cpu_dump_file             (char *);
void          cpu_load_options          (int *options);
void          cpu_exec                  (void);

#endif
