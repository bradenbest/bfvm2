#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>

#include "cpudefs.h"

#define SILENCE_UNUSED_ARG(arg) \
    ((void)(arg))

#define MIN(a, b) \
    ( (a) < (b) ? (a) : (b) )

#define MAX(a, b) \
    ( (a) > (b) ? (a) : (b) )

#define fopen_safe(fname, mode) \
    ( ((fname) == NULL) ? (NULL) : (fopen( (fname), (mode) ))  )

extern FILE *outfile;

u32     atou32                (char const *);
char ** putsv                 (char **sv);
u8      get_argsz             (u32 arg);
void    outfile_write_header  (u32 code_segment_size, u32 data_segment_size);
void    file_seek_to          (FILE *, char);
void    or_die                (char const *msg, int expr);

#endif
