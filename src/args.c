#include <stdio.h>
#include <stdlib.h>

#include "args.h"
#include "util.h"

static char ** parse_flag(char **args, struct arg_flag *);
static char ** parse_arg(char **args, struct arg_flag *);

static int ignore_flags;

static char **
parse_flag(char **args, struct arg_flag *flags)
{
    char *selarg = (*args) + 1;

    if(*selarg == '-')
        ignore_flags = 1;

    for(struct arg_flag *selflag = flags; !selflag->end; ++selflag)
        if(selflag->flag == *selarg)
            return selflag->callback(args + 1);

    return NULL;
}

static char **
parse_arg(char **args, struct arg_flag *flags)
{
    if(*args == NULL)
        return NULL;

    if(**args == '-' && !ignore_flags)
        return parse_flag(args, flags + 1);

    return flags->callback(args);
}

void
args_usage(char **lines, struct arg_flag *flags)
{
    char **selline = lines;

    selline = putsv(selline) + 1;

    for(struct arg_flag *selflag = flags + 1; !selflag->end; ++selflag)
        printf("-%c %s\n", selflag->flag, selflag->desc);

    printf("-- interpret flags as literal arguments after this\n");
    putsv(selline);
    exit(1);
}

void
args_parse(char **args, struct arg_flag *flags)
{
    while((args = parse_arg(args, flags)) != NULL)
        ;
}
