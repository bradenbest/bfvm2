#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "cpu.h"
#include "bfvm.h"
#include "util.h"
#include "version.h"

static u8 *cpu_pmem;
static u8 *cpu_pcnt;
static u8 *cpu_vmem;
static int cpu_errno;
static int bfvm_options[BFVM_OPT_END];

static char *cpu_errno_str[CPUERR_END] = {
    "OK.",
    "No filename provided. (filename == NULL)",
    "fopen() failed to open file.",
    "File is not a valid bfvm2 binary. Use `-h` to see usage help.",
    "File header version doesn't match bfvm version " VERSION_BFVM,
    "Something went wrong reading the file (check ferror())",
    "Total memory cannot exceed 1 GiB (1,048,576 KiB)\n",
    "malloc() failed to obtain memory",
};

static int   disassemble_instruction  (FILE *);
static void  dump_bytes               (u8 *buffer, size_t n);
static void  dump_header              (u8 *header);
static int   cpu_set_size             (size_t pcnt_sz_bytes, size_t vmem_sz_bytes);
static u8 *  exec_op                  (u8 *pc, u8 **vmp);

size_t cpu_pcnt_sz = PCNT_SZ_DEFAULT;
size_t cpu_vmem_sz = VMEM_SZ_DEFAULT;

char *cpu_opcode_str[OP_END] = {
    "NOP",
    "ADD",
    "SUB",
    "PSH",
    "POP",
    "JEZ",
    "JMP",
    "GET",
    "PUT",
    "EXT",
};

static int
disassemble_instruction(FILE *fp)
{
    u8 byte;
    u8 op;
    u8 size;
    u32 arg = 0;
    int ch;
    u8 nbuffer[4];
    size_t nread;

    if((ch = fgetc(fp)) == EOF)
        return 0;

    byte = (u8)ch;
    op = byte >> 4;
    size = byte & 0x0f;

    if((nread = fread(nbuffer, 1, size, fp)) != size)
        return 0;

    for(int i = size; i > 0; --i)
        arg = (arg << 8) | nbuffer[i - 1];

    if(arg < 2)
        printf("%s\n", cpu_opcode_str[op]);
    else
        printf("%s %u\n", cpu_opcode_str[op], arg);

    return 1;
}

static void
dump_bytes(u8 *buffer, size_t n)
{
    static int nch;

    for(size_t i = 0; i < n; ++i){
        if(nch % 16 == 0 && nch > 0)
            putchar('\n');

        printf("%02x ", buffer[i]);
        ++nch;
    }
}

static void
dump_header(u8 *header)
{
    static char const *compat_str[2] = { "not compatible", "compatible" };
    u32 codesz = (header[4]) | (header[5] << 8) | (header[6] << 16);
    u32 datasz = (header[7]) | (header[8] << 8) | (header[9] << 16);
    u8 iscompat = *(header + BINARY_FMT_VERSION_OFFSET) == BINARY_FMT_VERSION;

    printf("Header magic:      %02x %02x %02x\n", header[0], header[1], header[2]);
    printf("Header version:    %u (%s)\n", header[3], compat_str[iscompat]);
    printf("bfvm version:      %s\n", VERSION_BFVM);
    printf("Code segment size: %u KiB\n", codesz);
    printf("Data segment size: %u KiB\n", datasz);
}

/* sizes taken in bytes */
static int
cpu_set_size(size_t pcnt_sz_bytes, size_t vmem_sz_bytes)
{
    if(pcnt_sz_bytes + vmem_sz_bytes > GiB){
        cpu_errno = CPUERR_TOOBIG;
        return 0;
    }

    if(pcnt_sz_bytes > 0)
        cpu_pcnt_sz = pcnt_sz_bytes;

    if(vmem_sz_bytes > 0)
        cpu_vmem_sz = vmem_sz_bytes;

    return 1;
}

static u8 *
exec_op(u8 *pc, u8 **vmp)
{
    u8 opcode = *pc >> 4;
    u8 argsz = *pc & 0x0f;
    u32 arg = 0;
    static u32 cycleno = 1;

    if(argsz == 0)
        arg = 1;

    for(int i = argsz; i > 0; --i)
        arg = (arg << 8) | pc[i];

    if(bfvm_options[BFVM_OPT_VERBOSE]){
        puts("");
        printf("%03u pc:     %08lx\n", cycleno, pc - cpu_pcnt);
        printf("    vm:     %08lx = %02x (%u / %c)\n", *vmp - cpu_vmem, **vmp, **vmp, **vmp);
        printf("    opcode: %u (%s)\n", opcode, cpu_opcode_str[opcode]);
        printf("    arg:    %u (%u bytes)\n", arg, argsz);
        ++cycleno;
    }

    switch(opcode){
        case OP_NOP:
        default:
            break;

        case OP_ADD:
            **vmp += arg;
            break;

        case OP_SUB:
            **vmp -= arg;
            break;

        case OP_PSH:
            *vmp += arg;
            break;

        case OP_POP:
            *vmp -= arg;
            break;

        case OP_JEZ:
            if(**vmp == 0)
                return pc + arg;

            break;

        case OP_JMP:
            return pc - arg;

        case OP_GET:
            if(bfvm_options[BFVM_OPT_UNREALISTIC])
                for(u32 i = 0; i < arg; ++i)
                    **vmp = getchar();
            else
                **vmp = getchar();
            break;

        case OP_PUT:
            if(bfvm_options[BFVM_OPT_UNREALISTIC])
                for(u32 i = 0; i < arg; ++i)
                    putchar(**vmp);
            else
                putchar(**vmp);
            break;

        case OP_EXT:
            return NULL;
    }

    return pc + 1 + argsz;
}

int
cpu_init(void)
{
    cpu_pmem = malloc(PMEM_SZ);

    if(cpu_pmem == NULL){
        cpu_errno = CPUERR_MALLOCFAIL;
        return 0;
    }

    cpu_pcnt = cpu_pmem + 0;
    cpu_vmem = cpu_pmem + cpu_pcnt_sz;
    memset(cpu_vmem, 0, cpu_vmem_sz);
    return 1;
}

int
cpu_check_header(char *buffer)
{
    if(memcmp(buffer, BINARY_FMT_MAGIC, 3) != 0){
        cpu_errno = CPUERR_BADHEADER;
        return 0;
    }

    return 1;
}

int
cpu_check_header_version(char *buffer)
{
    if(*(buffer + BINARY_FMT_VERSION_OFFSET) != BINARY_FMT_VERSION){
        cpu_errno = CPUERR_OLDVERSION;
        return 0;
    }

    return 1;
}

int
cpu_set_size_via_header(char *header)
{
    size_t pcnt_sz_kib = 0;
    size_t vmem_sz_kib = 0;

    for(int i = 2; i >= 0; --i)
        pcnt_sz_kib = (pcnt_sz_kib << 8) | header[i];

    header += 3;

    for(int i = 2; i >= 0; --i)
        vmem_sz_kib = (vmem_sz_kib << 8) | header[i];

    return cpu_set_size_via_args(pcnt_sz_kib, vmem_sz_kib);
}

int
cpu_set_size_via_args(size_t pcnt_sz_kib, size_t vmem_sz_kib)
{
    return cpu_set_size(pcnt_sz_kib * KiB, vmem_sz_kib * KiB);
}

char const *
cpu_get_error(void)
{
    return cpu_errno_str[cpu_errno];
}

int
cpu_load_file(char *filename)
{
    u8 *pc;
    FILE *fp;
    size_t nread;
    char header[BINARY_FMT_HEADER_SIZE];

    if(filename == NULL){
        cpu_errno = CPUERR_NOFILE;
        return 0;
    }

    if((fp = fopen(filename, "rb")) == NULL){
        cpu_errno = CPUERR_FOPENFAIL;
        return 0;
    }

    /* under -f, the vm will assume no header and attempt to execute directly as bytecode */
    if(bfvm_options[BFVM_OPT_FORCE] == 0){
        if(fread(header, 1, BINARY_FMT_HEADER_SIZE, fp) < BINARY_FMT_HEADER_SIZE){
            cpu_errno = CPUERR_BADHEADER;
            return 0;
        }

        /* cpu_check_header and cpu_check_header_version set errno */
        if(cpu_check_header(header) == 0 || cpu_check_header_version(header) == 0)
            return 0;

        if(bfvm_options[BFVM_OPT_IGNORE_FILE_SIZE] == 0)
            if(cpu_set_size_via_header(header + BINARY_FMT_SIZE_OFFSET) == 0)
                return 0;
    }

    if(cpu_init() == 0)
        return 0;

    pc = cpu_pcnt;

    while((nread = fread(pc, 1, MIN(8 * KiB, cpu_pcnt_sz - (pc - cpu_pcnt)), fp)) > 0)
        pc += nread;

    if(ferror(fp)){
        cpu_errno = CPUERR_FERROR;
        return 0;
    }

    fclose(fp);
    return 1;
}

int
cpu_disassemble_file(char *filename)
{
    FILE *fp;
    char header[BINARY_FMT_HEADER_SIZE];
    size_t nread;

    if(filename == NULL){
        cpu_errno = CPUERR_NOFILE;
        return 0;
    }

    if((fp = fopen(filename, "rb")) == NULL){
        cpu_errno = CPUERR_FOPENFAIL;
        return 0;
    }

    if((nread = fread(header, 1, BINARY_FMT_HEADER_SIZE, fp)) < BINARY_FMT_HEADER_SIZE || cpu_check_header(header) == 0){
        rewind(fp);
        puts("%NOHDR");
    }

    while(disassemble_instruction(fp))
        ;

    fclose(fp);
    return 1;
}

int
cpu_dump_file(char *filename)
{
    FILE *fp;
    u8 header[BINARY_FMT_HEADER_SIZE];
    u8 buffer[4 * KiB];
    size_t nread;

    if(filename == NULL){
        cpu_errno = CPUERR_NOFILE;
        return 0;
    }

    if((fp = fopen(filename, "rb")) == NULL){
        cpu_errno = CPUERR_FOPENFAIL;
        return 0;
    }

    if((nread = fread(header, 1, BINARY_FMT_HEADER_SIZE, fp)) < 10 || cpu_check_header((char *)header) == 0){
        puts("No header");
        rewind(fp);
    }

    if(cpu_check_header((char *)header))
        dump_header(header);

    puts("Code segment dump:");

    while((nread = fread(buffer, 1, 4 * KiB, fp)) > 0)
        dump_bytes(buffer, nread);

    putchar('\n');

    fclose(fp);
    return 1;
}

void
cpu_load_options(int *options)
{
    for(int i = 0; i < BFVM_OPT_END; ++i)
        bfvm_options[i] = options[i];
}

void
cpu_exec(void)
{
    u8 *pc = cpu_pcnt;
    u8 *vm = cpu_vmem;

    if(bfvm_options[BFVM_OPT_VERBOSE]){
        printf("bfvm: pc size=%lu KiB (%lu bytes)\n", cpu_pcnt_sz / KiB, cpu_pcnt_sz);
        printf("bfvm: vm size=%lu KiB (%lu bytes)\n", cpu_vmem_sz / KiB, cpu_vmem_sz);
        puts("bfvm: Launching VM");
    }

    while((pc = exec_op(pc, &vm)) != NULL)
        ;

    free(cpu_pmem);
}
