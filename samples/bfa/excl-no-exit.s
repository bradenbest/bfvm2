# exclamation with no exit instruction at the end

%CODESZ 1
%DATASZ 1
ADD 5
JEZ 9
PSH
ADD 6
POP
SUB
JMP 7
PSH
ADD 3
PUT

# without an EXT instruction, the code will run off the end until it either encounters a byte starting with 1001XXXX
# (as 9 is the opcode for EXT) or it runs off the end of the data segment. Once it goes beyond the end
# of the data segment, the behavior is undefined
