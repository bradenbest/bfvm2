# exclamation but with 16GiB code and data segments for a total of
# 32GiB physical memory
# the limit for bfvm's physical memory is 1 GiB.
# if the code and data segment combined exceed this limit, the vm will
# refuse to run, even with -f

%CODESZ 16777215
%DATASZ 16777215
ADD 4294967295
JEZ 9
PSH
ADD 6
POP
SUB
JMP 7
PSH
ADD 3
PUT
EXT
