# exclamation without a header
# the %NOHDR directive tells the assembler not to emit a header
# it will emit a raw binary, which can only be run with `bfvm -f`

%NOHDR
ADD 5
JEZ 9
PSH
ADD 6
POP
SUB
JMP 7
PSH
ADD 3
PUT
EXT
