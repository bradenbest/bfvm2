[
See commit b5bf25075268b3506a46b52f82c5b8d80fe0625e

Make sure to read the vm executable specification (`bfvm2-executable.md`)
if you don't understand the low-level talk.

The instructions of the program total to 1024 bytes after compilation.
The EXT instruction, added by the compiler, totals it to 1025 bytes.
Prior to the patch in the commit, the compiler will not factor in the
EXT nor the first instruction, resulting in an erroneous count of 1022
and thus a code segment size of 1 KiB. On load, the EXT instruction will
be truncated, which will cause unexpected things to happen

This program tests that edge case.

Assume the program is compiled normally and executed in unrealistic mode

    bfc samples/bf/test-clist_get_total_program_size.bf
    bfvm a.out -u

The program sets cells 1, 3, and 4 to 130, 70, and 150, respectively.
It then skips through a bunch of loops, eventually printing out "F".
This is how this program behaves in a typical brainfuck implementation.

Prior to the patch, what will happen is it will print 17,921 F's. There
is nothing wrong with the program nor machine logic. What's happening
is an example of unintentional arbitrary code execution. Since the
program takes up 1025 bytes but the compiler calculated the wrong size,
the compiler emits a code segment size of 1 into the binary, which
causes the VM to allocate 1 KiB for the code segment, which causes the
EXT instruction to be omitted, as 1025 bytes will not fit into a 1024-
byte segment. When the program gets to the PUT instruction at the end,
the memory will look like this:

    80   00 82 00 46 96 00 00 00 00 00 00 ...
    ^  ^          ^
    |  |          (data pointer position)
    |  (end of code segment; start of data segment)
    (program counter position)

Since the EXT was truncated, the program counter will run off the end,
into the data segment and interpret these byes as code:

    NOP
    PUT 17920
    EXT

`00` is NOP, which is skipped

`82` is PUT with a 2-byte argument

`00 46` is its argument. Since BFVM is little-endian, the number is
interpreted as 0x4600, or 17,920. Which means the value at the data
pointer (0x46 'F') will be printed 17,920 additional times (in
unrealistic mode. In normal mode, it prints once).

`96` is EXT with a 6-byte argument. The VM shuts down immediately when
it sees this.

`00 00 00 00 00 00` is its argument. It is completely ignored, although
if it were to run past the end of the data segment, the behavior would
be undefined.

Correct output (no ACE): "F"
Incorrect output (ACE): "FF", or "F" 17921 times

The only thing that prevents this exploit from working post-patch is
the EXT at the end. Manually removing the EXT will cause the ACE exploit
to work. Try compiling with `bfc -S program.bf` to emit assembly, and
removing the EXT at the end before assembling with `bfas a.s`, for example.
]

20 bytes: (ACE setup)
    PSH 2    >>
    ADD 10   +++++ +++++
    JEZ 16   [
    PSH          >
    ADD 7        +++++ ++
    PSH          >
    ADD 15       +++++ +++++ +++++
    POP 3        <<<
    ADD 13       +++++ +++++ +++
    PSH          >
    SUB          -
    JMP 14   ]

1000 bytes: (filler)
    JEZ 4   [][][][][] [][][][][] [][][][][] [][][][][] [][][][][]
    JMP 2   [][][][][] [][][][][] [][][][][] [][][][][] [][][][][]
    x 250   [][][][][] [][][][][] [][][][][] [][][][][] [][][][][]
            [][][][][] [][][][][] [][][][][] [][][][][] [][][][][]
            [][][][][] [][][][][] [][][][][] [][][][][] [][][][][]
            [][][][][] [][][][][] [][][][][] [][][][][] [][][][][]
            [][][][][] [][][][][] [][][][][] [][][][][] [][][][][]
            [][][][][] [][][][][] [][][][][] [][][][][] [][][][][]
            [][][][][] [][][][][] [][][][][] [][][][][] [][][][][]
            [][][][][] [][][][][] [][][][][] [][][][][] [][][][][]

2 bytes: (filler)
    PSH     >
    POP     <

2 bytes: (print and ACE runoff point)
    PSH     >
    PUT     .
