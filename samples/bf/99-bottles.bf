[comment
    99 bottles by Braden Best.
    Thus far the most complex brainfuck program I've ever written, with
    data structures (C-strings) and extensive documentation
]
[comment
    layout

    1   counter (10 bytes)
            counter
            buffer 1
            buffer 2
            buffer 3
            buffer 4
            buffer 5
            buffer 6
            buffer 7
            buffer 8
            buffer 9
    2   punctuation (4 bytes)
            space; comma; period; newline
    3   "bottles of beer"
    4   "on the wall"
    5   "Take one down, pass it around, "
    6   "Nno more "
    7   "Go to the store and buy some more, 99 "
    8
]

[ comment
    Number printer courtesy of esolangs.org
    https://esolangs.org/wiki/brainfuck_algorithms#Print_value_of_cell_x_as_number_.288-bit.29
    Uses 9 buffers, restores N and stops pointer at N.

    >>++++++++++<<[->+>-[>+>>]>[+[-<+>]>+>>]<<<<<<]>>[-]>>>++++++++++<[->-
    [>+>>]>[+[-<+>]>+>>]<<<<<]>[-]>>[>++++++[-<++++++++>]<.<<+>+>[-]]<[<[-
    >-<]++++++[->++++++++<]>.[-]]<<++++++[-<++++++++>]<.[-]<<[-<+>]<
]

[section: data]

sixteens
+++++ +++++
+++++ +
[
    00 > _____ _____ stop 1

    63 > +++++ +____ counter
    xx > +____ _____ buffer 1 (buffers are nonzero so the stops can work correctly during setup)
    xx > +____ _____ buffer 2
    xx > +____ _____ buffer 3
    xx > +____ _____ buffer 4
    xx > +____ _____ buffer 5
    xx > +____ _____ buffer 6
    xx > +____ _____ buffer 7
    xx > +____ _____ buffer 8
    xx > +____ _____ buffer 9
    00 > _____ _____ stop 2

    20 > ++___ _____ space
    2c > +++__ _____ comma
    2e > +++__ _____ period
    0a > +____ _____ newline
    00 > _____ _____ stop 3

    62 > +++++ +____ b
    6f > +++++ ++___ o
    74 > +++++ ++___ t
    74 > +++++ ++___ t
    6c > +++++ ++___ l
    65 > +++++ +____ e
    73 > +++++ ++___ s
    20 > ++___ _____ space
    6f > +++++ ++___ o
    66 > +++++ +____ f
    20 > ++___ _____ space
    62 > +++++ +____ b
    65 > +++++ +____ e
    65 > +++++ +____ e
    72 > +++++ ++___ r
    00 > _____ _____ stop 4

    6f > +++++ ++___ o
    6e > +++++ ++___ n
    20 > ++___ _____ space
    74 > +++++ ++___ t
    68 > +++++ +____ h
    65 > +++++ +____ e
    20 > ++___ _____ space
    77 > +++++ ++___ w
    61 > +++++ +____ a
    6c > +++++ ++___ l
    6c > +++++ ++___ l
    00 > _____ _____ stop 5

    54 > +++++ _____ T
    61 > +++++ +____ a
    6b > +++++ ++___ k
    65 > +++++ +____ e
    20 > ++___ _____ space
    6f > +++++ ++___ o
    6e > +++++ ++___ n
    65 > +++++ +____ e
    20 > ++___ _____ space
    64 > +++++ +____ d
    6f > +++++ ++___ o
    77 > +++++ ++___ w
    6e > +++++ ++___ n
    2c > +++__ _____ comma
    20 > ++___ _____ space
    70 > +++++ ++___ p
    61 > +++++ +____ a
    73 > +++++ ++___ s
    73 > +++++ ++___ s
    20 > ++___ _____ space
    69 > +++++ +____ i
    74 > +++++ ++___ t
    20 > ++___ _____ space
    61 > +++++ +____ a
    72 > +++++ ++___ r
    6f > +++++ ++___ o
    75 > +++++ ++___ u
    6e > +++++ ++___ n
    64 > +++++ +____ d
    2c > +++__ _____ comma
    20 > ++___ _____ space
    00 > _____ _____ stop 6

    4e > +++++ _____ N
    6e > +++++ ++___ n
    6f > +++++ ++___ o
    20 > ++___ _____ space
    6d > +++++ ++___ m
    6f > +++++ ++___ o
    72 > +++++ ++___ r
    65 > +++++ +____ e
    20 > ++___ _____ space
    00 > _____ _____ stop 7

    47 > ++++_ _____ G
    6f > +++++ ++___ o
    20 > ++___ _____ space
    74 > +++++ ++___ t
    6f > +++++ ++___ o
    20 > ++___ _____ space
    74 > +++++ ++___ t
    68 > +++++ +____ h
    65 > +++++ +____ e
    20 > ++___ _____ space
    73 > +++++ ++___ s
    74 > +++++ ++___ t
    6f > +++++ ++___ o
    72 > +++++ ++___ r
    65 > +++++ +____ e
    20 > ++___ _____ space
    61 > +++++ +____ a
    6e > +++++ ++___ n
    64 > +++++ +____ d
    20 > ++___ _____ space
    62 > +++++ +____ b
    75 > +++++ ++___ u
    79 > +++++ ++___ y
    20 > ++___ _____ space
    73 > +++++ ++___ s
    6f > +++++ ++___ o
    6d > +++++ ++___ m
    65 > +++++ +____ e
    20 > ++___ _____ space
    6d > +++++ ++___ m
    6f > +++++ ++___ o
    72 > +++++ ++___ r
    65 > +++++ +____ e
    2c > +++__ _____ comma
    20 > ++___ _____ space
    39 > +++__ _____ 9
    39 > +++__ _____ 9
    20 > ++___ _____ space
    00 > _____ _____ stop 8

    stop 8 to stop 1 (7 stops)
    <[<] <[<] <[<] <[<] <[<]
    <[<] <[<]
<-]

ones
    00 > _____ _____ stop 1

    63 > +++__ _____ counter
    xx > _____ _____ buffer 1
    xx > _____ _____ buffer 2
    xx > _____ _____ buffer 3
    xx > _____ _____ buffer 4
    xx > _____ _____ buffer 5
    xx > _____ _____ buffer 6
    xx > _____ _____ buffer 7
    xx > _____ _____ buffer 8
    xx > _____ _____ buffer 9
    00 > _____ _____ stop 2

    20 > _____ _____ space
    2c > ----_ _____ comma
    2e > --___ _____ period
    0a > ----- -____ newline
    00 > _____ _____ stop 3

    62 > ++___ _____ b
    6f > -____ _____ o
    74 > ++++_ _____ t
    74 > ++++_ _____ t
    6c > ----_ _____ l
    65 > +++++ _____ e
    73 > +++__ _____ s
    20 > _____ _____ space
    6f > -____ _____ o
    66 > +++++ +____ f
    20 > _____ _____ space
    62 > ++___ _____ b
    65 > +++++ _____ e
    65 > +++++ _____ e
    72 > ++___ _____ r
    00 > _____ _____ stop 4

    6f > -____ _____ o
    6e > --___ _____ n
    20 > _____ _____ space
    74 > ++++_ _____ t
    68 > +++++ +++__ h
    65 > +++++ _____ e
    20 > _____ _____ space
    77 > +++++ ++___ w
    61 > +____ _____ a
    6c > ----_ _____ l
    6c > ----_ _____ l
    00 > _____ _____ stop 5

    54 > ++++_ _____ T
    61 > +____ _____ a
    6b > ----- _____ k
    65 > +++++ _____ e
    20 > _____ _____ space
    6f > -____ _____ o
    6e > --___ _____ n
    65 > +++++ _____ e
    20 > _____ _____ space
    64 > ++++_ _____ d
    6f > -____ _____ o
    77 > +++++ ++___ w
    6e > --___ _____ n
    2c > ----_ _____ comma
    20 > _____ _____ space
    70 > _____ _____ p
    61 > +____ _____ a
    73 > +++__ _____ s
    73 > +++__ _____ s
    20 > _____ _____ space
    69 > +++++ ++++_ i
    74 > ++++_ _____ t
    20 > _____ _____ space
    61 > +____ _____ a
    72 > ++___ _____ r
    6f > -____ _____ o
    75 > +++++ _____ u
    6e > --___ _____ n
    64 > ++++_ _____ d
    2c > ----_ _____ comma
    20 > _____ _____ space
    00 > _____ _____ stop 6

    4e > --___ _____ N
    6e > --___ _____ n
    6f > -____ _____ o
    20 > _____ _____ space
    6d > ---__ _____ m
    6f > -____ _____ o
    72 > ++___ _____ r
    65 > +++++ _____ e
    20 > _____ _____ space
    00 > _____ _____ stop 7

    47 > +++++ ++___ G
    6f > -____ _____ o
    20 > _____ _____ space
    74 > ++++_ _____ t
    6f > -____ _____ o
    20 > _____ _____ space
    74 > ++++_ _____ t
    68 > +++++ +++__ h
    65 > +++++ _____ e
    20 > _____ _____ space
    73 > +++__ _____ s
    74 > ++++_ _____ t
    6f > -____ _____ o
    72 > ++___ _____ r
    65 > +++++ _____ e
    20 > _____ _____ space
    61 > +____ _____ a
    6e > --___ _____ n
    64 > ++++_ _____ d
    20 > _____ _____ space
    62 > ++___ _____ b
    75 > +++++ _____ u
    79 > +++++ ++++_ y
    20 > _____ _____ space
    73 > +++__ _____ s
    6f > -____ _____ o
    6d > ---__ _____ m
    65 > +++++ _____ e
    20 > _____ _____ space
    6d > ---__ _____ m
    6f > -____ _____ o
    72 > ++___ _____ r
    65 > +++++ _____ e
    2c > ----_ _____ comma
    20 > _____ _____ space
    39 > +++++ ++++_ 9
    39 > +++++ ++++_ 9
    20 > _____ _____ space
    00 > _____ _____ stop 8

    stop 8 to stop 1 (7 stops)
    <[<] <[<] <[<] <[<] <[<]
    <[<] <[<]

[section: code]

    [stop 1]

jump to counter
>

set all buffers to zero (9)
>[-] >[-] >[-] >[-] >[-]
>[-] >[-] >[-] >[-]

jump back to counter (9)
<<<<< <<<<

start of main loop
[
    Number printer
        >>++++++++++<<[->+>-[>+>>]>[+[-<+>]>+>>]<<<<<<]>>[-]>>>++++++++++<[->-
        [>+>>]>[+[-<+>]>+>>]<<<<<]>[-]>>[>++++++[-<++++++++>]<.<<+>+>[-]]<[<[-
        >-<]++++++[->++++++++<]>.[-]]<<++++++[-<++++++++>]<.[-]<<[-<+>]<
    counter to stop 2 (10)
    >>>>> >>>>>

    [stop 2]
        print space
        >.
        stop 2 to stop 3 (1)
        [>]

    [stop 3]
        print string til stop 4
        >[.>]

    [stop 4]
        stop 4 to stop 2 (2)
        <[<] <[<]

    [stop 2]
        print space
        >.
        stop 2 to stop 4 (2)
        [>] >[>]

    [stop 4]
        print string til stop 5
        >[.>]

    [stop 5]
        stop 5 to stop 2 (3)
        <[<] <[<] <[<]

    [stop 2]
        print comma
        >>.
        print space
        <.
        back to stop 2
        [<]

    [stop 2]
        [comment
            finished line: "N bottles of beer on the wall, "
        ]
        stop 2 to counter (10)
        <<<<< <<<<<
        Number printer
            >>++++++++++<<[->+>-[>+>>]>[+[-<+>]>+>>]<<<<<<]>>[-]>>>++++++++++<[->-
            [>+>>]>[+[-<+>]>+>>]<<<<<]>[-]>>[>++++++[-<++++++++>]<.<<+>+>[-]]<[<[-
            >-<]++++++[->++++++++<]>.[-]]<<++++++[-<++++++++>]<.[-]<<[-<+>]<
        counter to stop 2 (10)
        >>>>> >>>>>

    [stop 2]
        print space
        >.
        forward to stop 3 (1)
        [>]

    [stop 3]
        print string til stop 4
        >[.>]

    [stop 4]
        stop 4 to stop 2 (2)
        <[<] <[<]

    [stop 2]
        print period
        >>>.
        print newline
        >.
        forward to stop 3
        [>]

    [stop 3]
        [comment
            finished line: "N bottles of beer.\n"
        ]
        stop 3 to stop 5 (2)
        >[>] >[>]

    [stop 5]
        print string til stop 6
        >[.>]

    [stop 6]
        [comment
            finished line: "Take one down, pass it around, "
        ]
        stop 6 to stop 2 (4)
        <[<] <[<] <[<] <[<]

    [stop 2]
        stop 2 to counter (10)
        <<<<< <<<<<
        decrement counter (for loop control and for the last part of the verse)
        -
        Number printer
            >>++++++++++<<[->+>-[>+>>]>[+[-<+>]>+>>]<<<<<<]>>[-]>>>++++++++++<[->-
            [>+>>]>[+[-<+>]>+>>]<<<<<]>[-]>>[>++++++[-<++++++++>]<.<<+>+>[-]]<[<[-
            >-<]++++++[->++++++++<]>.[-]]<<++++++[-<++++++++>]<.[-]<<[-<+>]<
        counter to stop 2 (10)
        >>>>> >>>>>

    [stop 2]
        print space
        >.
        forward to stop 3
        [>]

    [stop 3]
        print string til stop 4
        >[.>]

    [stop 4]
        stop 4 to stop 2 (2)
        <[<] <[<]

    [stop 2]
        print space
        >.
        forward to stop 3
        [>]

    [stop 3]
        stop 3 to stop 4 (1)
        >[>]

    [stop 4]
        print string til stop 5
        >[.>]

    [stop 5]
        stop 5 to stop 2 (3)
        <[<] <[<] <[<]

    [stop 2]
        print period
        >>>.
        print 2 newlines
        >..
        back to stop 2
        [<]

    [stop 2]
        [comment
            finished line: "N bottles of beer on the wall.\n\n"
            finished verse
        ]
        stop 2 to counter (10)
        <<<<< <<<<<

end of main loop
decrement happens before third call to Number printer
]

[comment
    for completeness, we will now print the
    "No more bottles of beer on the wall" verse
]

counter to stop 2 (10)
>>>>> >>>>>

[stop 2]
    stop 2 to stop 6 (4)
    >[>] >[>] >[>] >[>]

[stop 6]
    print N
    >.
    print rest of string til stop 7
    >>[.>]

[stop 7]
    stop 7 to stop 3 (4)
    <[<] <[<] <[<] <[<]

[stop 3]
    print string til stop 4
    >[.>]

[stop 4]
    stop 4 to stop 2 (2)
    <[<] <[<]

[stop 2]
    print space
    >.
    forward to stop 3
    [>]

[stop 3]
    stop 3 to stop 4 (1)
    >[>]

[stop 4]
    print string til stop 5
    >[.>]

[stop 5]
    stop 5 to stop 2 (3)
    <[<] <[<] <[<]

[stop 2]
    print comma
    >>.
    print space
    <.
    foward to stop 3
    [>]

[stop 3]
    [comment
        finished line: "No more bottles of beer on the wall, "
    ]
    stop 3 to stop 6 (3)
    >[>] >[>] >[>]

[stop 6]
    print string from n til stop 7
    >>[.>]

[stop 7]
    stop 7 to stop 3 (4)
    <[<] <[<] <[<] <[<]

[stop 3]
    print string til stop 4
    >[.>]

[stop 4]
    stop 4 to stop 2 (2)
    <[<] <[<]

[stop 2]
    print period
    >>>.
    print newline
    >.
    forward to stop 3
    [>]

[stop 3]
    [comment
        finished line: "no more bottles of beer.\n"
    ]
    stop 3 to stop 7 (4)
    >[>] >[>] >[>] >[>]

[stop 7]
    print line til stop 8
    >[.>]
    [comment
        finished line: "Go to the store and buy some more, 99 "
    ]

[stop 8]
    stop 8 to stop 3 (5)
    <[<] <[<] <[<] <[<] <[<]

[stop 3]
    print string til stop 4
    >[.>]

[stop 4]
    stop 4 to stop 2 (2)
    <[<] <[<]

[stop 2]
    print space
    >.
    forward to stop 3
    [>]

[stop 3]
    stop 3 to stop 4 (1)
    >[>]

[stop 4]
    print string til stop 5
    >[.>]

[stop 5]
    stop 5 to stop 2 (3)
    <[<] <[<] <[<]

[stop 2]
    print period
    >>>.
    print newline
    >.
    forward to stop 3
    [>]

[stop 3]
    [comment
        finished line: "bottles of beer on the wall.\n"
        end of program
    ]
