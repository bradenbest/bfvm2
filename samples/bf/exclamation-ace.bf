[%DATASZ 1]
[%NOEXT]
[
emits the code segment of exclamation.bfo into the data segment of the VM
The %DATASZ and %NOEXT above tell the compiler:
* what size to set the data segment to (32KiB default)
* to not emit en EXT instruction
So the program counter will run off into the data segment and execute
this data segment as code. I.e. arbitrary code execution
This bf program will not do anything interesting in a regular bf interpreter
It will just set a bunch of cells to the machine code
]
set opcodes
+++++ +++++
+++++ +
[
    >
    +          > 10
               > 00
    +++++      > 50
               > 00
    +++        > 30
    +          > 10
               > 00
    ++++       > 40
    ++         > 20
    +++++ +    > 60
               > 00
    +++        > 30
    +          > 10
               > 00
    +++++ +++  > 80
    +++++ ++++ > 90
    <<<<< <<<<<
    <<<<< <
    <-
]
>

set arguments
+         > 11 ADD
+++++     > 05 5
+         > 51 JEZ
+++++ ++++> 09 9
          > 30 PSH
+         > 11 ADD
+++++ +   > 06 6
          > 40 POP
          > 20 SUB
+         > 61 JMP
+++++ ++  > 07 7
          > 30 PSH
+         > 11 ADD
+++       > 03 3
          > 80 PUT
          > 90 EXT
